package com.example.greggarnhart.amazebygreggarnhart.gui;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.greggarnhart.amazebygreggarnhart.R;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import generation.GlobalClass;

// Song from http://bensound.com
// (It's royalty free so i promise we are good to publish this to the android store i am think 30000000 downloads minimum

public class AMazeActivity extends AppCompatActivity {

    public static final String CHOSEN_LEVEL = "com.example.greggarnhart.amazebygreggarnhart.Level";
    public static final String CHOSEN_ALGORITHM = "com.example.greggarnhart.amazebygreggarnhart.Algorithm";
    public static final String CHOSEN_DRIVER = "com.example.greggarnhart.amazebygreggarnhart.RobotDriver";
    public static final String REVISIT_BOOL = "com.example.greggarnhart.amazebygreggarnhart.Revisit";

    private static final String LEVEL_ZERO_SEED = "zeroMaze";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toggleNightMode();

        final MediaPlayer player = MediaPlayer.create(this, R.raw.intro);
        player.start();
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                player.release();
            }
        });
        // seekbar label
        final TextView difficultyLevelLabel = (TextView) findViewById(R.id.difficultySetting);

        // set seekBar min and max
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setMin(0);
        seekBar.setMax(15);

        // set algorithms
        Spinner algoSpinner = (Spinner) findViewById(R.id.algorithmSpinner);
        String[] algorithmsList = {"DFS", "Prim", "Ellers"}; //TODO: Make sure these are the only ones needed
        ArrayAdapter<String> algorithmAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, algorithmsList); // TODO figure out the spinner portion of this.
        algorithmAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        algoSpinner.setAdapter(algorithmAdapter);

        // set driver list
        Spinner driverSpinner = (Spinner) findViewById(R.id.driverSpinner);
        String[] driverList = {"Manual", "Wizard", "WallFollower", "Explorer"};
        ArrayAdapter<String> driverAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, driverList);
        driverAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        driverSpinner.setAdapter(driverAdapter);

        // setup seekbar change
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String level = Integer.toString(seekBar.getProgress());
                difficultyLevelLabel.setText(level);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    /**
     * Set seeds for random generation IF no seeds have been set thus far :)
     * Let's set the SEEDS and grow the wheat so we can get this bread.
     * ha
     */
    private void setSeeds() {

        // write them!
        String zero = "" + System.currentTimeMillis();

        Log.v("setting zero seed", zero);

        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = openFileOutput(LEVEL_ZERO_SEED, MODE_PRIVATE);
            fileOutputStream.write(zero.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != fileOutputStream) {
                try {
                    fileOutputStream.close(); // lol this is outrageous
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Called when the new maze button is pushed
     *
     * @param view
     */
    public void buildNewMaze(View view) {
        setSeeds();
        Intent intent = new Intent(this, GeneratingActivity.class);

        // get seekBar information
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
        int levelChosen = seekBar.getProgress();

        // get algorithm information
        Spinner algoSpinner = (Spinner) findViewById(R.id.algorithmSpinner);
        String chosenAlgorithm = algoSpinner.getSelectedItem().toString();

        // get driver information
        Spinner driverSpinner = (Spinner) findViewById(R.id.driverSpinner);
        String driverType = driverSpinner.getSelectedItem().toString();

        intent.putExtra(CHOSEN_LEVEL, Integer.toString(levelChosen));
        intent.putExtra(CHOSEN_ALGORITHM, chosenAlgorithm);
        intent.putExtra(CHOSEN_DRIVER, driverType);
        intent.putExtra(REVISIT_BOOL, "false");
        startActivity(intent);

        Log.v("Start Build Button", "Button Tapped, moving to Build Activity!");
    }

    public void revisitMaze(View view) {
        Intent intent = new Intent(this, GeneratingActivity.class);

        // get seekBar information
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
        int levelChosen = seekBar.getProgress();

        // get algorithm information
        Spinner algoSpinner = (Spinner) findViewById(R.id.algorithmSpinner);
        String chosenAlgorithm = algoSpinner.getSelectedItem().toString();

        // get driver information
        Spinner driverSpinner = (Spinner) findViewById(R.id.driverSpinner);
        String driverType = driverSpinner.getSelectedItem().toString();

        intent.putExtra(CHOSEN_LEVEL, Integer.toString(levelChosen));
        intent.putExtra(CHOSEN_ALGORITHM, chosenAlgorithm);
        intent.putExtra(CHOSEN_DRIVER, driverType);
        intent.putExtra(REVISIT_BOOL, "true");

        startActivity(intent);

        Log.v("Start Build Button", "Button Tapped, moving to Build Activity!");
    }

    public void toggleNightMode(View view){
        GlobalClass gc = (GlobalClass) getApplicationContext();
        gc.nightMode = !gc.nightMode;
        toggleNightMode();
    }

    public void toggleNightMode(){
        GlobalClass gc = (GlobalClass) getApplicationContext();
        String fontColor;
        String buttonColor;

        if(gc.nightMode){
            int deepBlue = Color.parseColor("#00303D");
            getWindow().getDecorView().setBackgroundColor(deepBlue);

            fontColor = "#F3F3F3";
            buttonColor = "#37CFCA";

        }
        else{
            getWindow().getDecorView().setBackgroundColor(Color.WHITE);
            fontColor = "#00303D";
            buttonColor = "#D6D7D7";

        }

        int fontColorInt = Color.parseColor(fontColor);
        int buttonColorInt = Color.parseColor(buttonColor);


        TextView difficultyLevelLabel = findViewById(R.id.difficultySetting);
        TextView algoLabel = findViewById(R.id.algorithmLabel);
        TextView difficultyLabel = findViewById(R.id.difficultyLabel);
        TextView driverLabel = findViewById(R.id.driverLabel);
        Button nightModeButton = findViewById(R.id.nightModeButton);
        Button revisitButton = findViewById(R.id.revisitButton);
        Button exploreButton = findViewById(R.id.exploreButton);
        Spinner driverSpinner = (Spinner) findViewById(R.id.driverSpinner);
        Spinner algoSpinner = (Spinner) findViewById(R.id.algorithmSpinner);


        difficultyLevelLabel.setTextColor(fontColorInt);
        algoLabel.setTextColor(fontColorInt);
        difficultyLabel.setTextColor(fontColorInt);
        driverLabel.setTextColor(fontColorInt);
        nightModeButton.setTextColor(fontColorInt);
        revisitButton.setTextColor(fontColorInt);
        exploreButton.setTextColor(fontColorInt);
        nightModeButton.setBackgroundColor(buttonColorInt);
        revisitButton.setBackgroundColor(buttonColorInt);
        exploreButton.setBackgroundColor(buttonColorInt);
    }

}
