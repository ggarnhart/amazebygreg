package com.example.greggarnhart.amazebygreggarnhart.gui;

import android.util.Log;

import generation.CardinalDirection;
import generation.MazeConfiguration;

public class BasicRobot implements Robot {


    /*
     * Add some instance variables. Current thoughts:
     * integer energy - calculates how much energy is left
     * 4 distance sensors
     *
     * Probably will need a Controller to update
     */

    private float batteryLevel;
    private int odometer;
    private CardinalDirection cardinalFacing; // this needs to be considered and set with directionFacing
    private float moveForwardCost;
    int [] position = new int[2];
    private MazeConfiguration mazeConfig;
    boolean inPlayingState;
    public int initalEnergy = 3000;


    private StateAnimation playState;
    /**
     * The array of sensors!
     * In the basic robot, 5 sensors are present. 4 directional sensors and a room sensor.
     * The direction sensors are stored in clockwise order (front, right, back, left) and the
     * room sensor is the last sensor in the array.
     */
    private Sensor [] sensors = new Sensor[5];

    /**
     * Hi this is a nice constructor friendo.
     * It sets up the directions and the batteryLevel and makes sure it exists.
     * Thanks, mister method.
     *
     * Some nice things to note:
     * the controller (this.maze) is set up in MazeApplication upon instantiation
     */
    public BasicRobot(StateAnimation playState, MazeConfiguration mazeConfig) {
        this.setBatteryLevel(initalEnergy);
        this.resetOdometer();
        this.moveForwardCost = 5;
        this.inPlayingState = false;
        this.playState = playState;
        this.mazeConfig = mazeConfig;
    }

    /**
     * Provides one of the most basic functions of a Robot's abilities: the coveted TURN.
     * This happens by changing the direction the robot is facing based on the requested turn parameter (see below).
     * If robot runs out of energy, it stops. This is important!
     * You can check this with hasStopped() == true and batteryLevel <= 0
     *
     * Calculate energy needed to turn
     * See if you can turn
     * (turn)
     *   Given the turn direction, rotate the proper amount of degrees and then reset which way it is facing.
     * set energy
     * update cardinal direction with updateCardinalDirection()
     *
     * Note: only let this happen during the correct state. Check that elsewhere, though.
     *
     * @param turn This is the direction of the turn. Note that it comes from the Robot.java interface
     */
    @Override
    public void rotate(Turn turn) {
        Log.v("BasicRobot Rotate","rotating robbie!");

        this.cardinalFacing = this.getCardinalDirection();
        if(this.batteryLevel >= getEnergyForRotation(turn) && this.playState.inPlayState()) {
            switch(turn) {

                case LEFT:
                    this.playState.rotate(1);
                    this.depleteEnergy(3);
                    this.cardinalFacing = this.playState.getCurrentDirection();
                    break;
                case RIGHT:
                    this.playState.rotate(-1);
                    this.depleteEnergy(3);
                    this.cardinalFacing = this.playState.getCurrentDirection();
                    break;
                case AROUND:
                    this.rotate(Turn.LEFT);
                    this.rotate(Turn.LEFT);
                    this.cardinalFacing = this.playState.getCurrentDirection();
                default:
                    break;

            }

        }
        else if(this.batteryLevel < getEnergyForRotation(turn)) {
            this.playState.checkGameOver(this.position[0], this.position[1], true);
        }

    }

    /**
     * Moves robot forward a given number of steps. A step matches a single cell.
     * If the robot runs out of energy somewhere on its way, it stops,
     * which can be checked by hasStopped() == true and by checking the battery level.
     *
     * If the robot hits an obstacle like a wall, it depends on the mode of operation
     * what happens. If an algorithm drives the robot, it remains at the position in front
     * of the obstacle and also hasStopped() == true as this is not supposed to happen.
     * This is also helpful to recognize if the robot implementation and the actual maze
     * do not share a consistent view on where walls are and where not.
     *
     * If a user manually operates the robot, this behavior is inconvenient for a user,
     * such that in case of a manual operation the robot remains at the position in front
     * of the obstacle but hasStopped() == false and the game can continue.
     *
     * execution:
     * check energy to see if you can move one step (the robot moves as far as it can, so it always checks)
     * move forward a step
     * check to see if you still need to move forward
     *
     *
     * @param distance is the number of cells to move in the robot's current forward direction
     * @param manual is true if robot is operated manually by user, false otherwise
     * @precondition distance >= 0
     */
    @Override
    public void move(int distance, boolean manual) {

        this.cardinalFacing = this.playState.getCurrentDirection();

        while(!this.hasStopped() && distance > 0 && this.batteryLevel >= this.getEnergyForStepForward()) {

            this.playState.walk(1);
            this.depleteEnergy(this.getEnergyForStepForward());
            this.odometer = this.odometer + 1;

            // update the coordinates!
            try {
                this.position = this.getCurrentPosition();
                Log.v("BasicRobot Move","moved robbie! New location: "+this.position[0]+" "+this.position[1]);
                // System.out.println("After the move, Robbie's coodinates are: "+ this.getCurrentPosition()[0]+", "+ this.getCurrentPosition()[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }

            distance = distance - 1;
        }

        // this happens if we get out of the loop before distance is 0.
        if(distance > 0 && this.batteryLevel == 0) {
            this.playState.checkGameOver(this.position[0], this.position[1], true);
        }
        this.position = this.playState.getCurrentPosition();
        this.playState.isOutside(this.position[0], this.position[1]);
    }

    /**
     * Provides the current position as (x,y) coordinates for the maze cell as an array of length 2 with [x,y].
     * @postcondition 0 <= x < width, 0 <= y < height of the maze.
     * @return array of length 2, x = array[0], y=array[1]
     * @throws Exception if position is outside of the maze
     *
     * pseudo-code not really needed here - it just returns the private variable
     */
    @Override
    public int[] getCurrentPosition() throws Exception {
        return this.position;
    }

    /**
     * MAY NOT BE USED as mazeconfigs will be global. just in case, though.
     * @param controller is the communication partner for robot
     */
    @Override
    public void setMaze(MazeConfiguration controller) {
        this.mazeConfig = controller;
    }



    /**
     * Tells if current position (x,y) is right at the exit but still inside the maze.
     * Used to recognize termination of a search.
     * @return true if robot is at the exit, false otherwise
     *
     * check to see if any of the four walls surrounding the cell are the exit
     * return true if that's the case
     *
     * FREE of charge.
     */
    @Override
    public boolean isAtExit() {
        boolean atExit = false;

        try {
            int [] currentCoordinates = this.getCurrentPosition();

            if(this.mazeConfig.getMazecells().isExitPosition(currentCoordinates[0], currentCoordinates[1])) {
                atExit = true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return atExit;
    }

    /**
     * Tells if a sensor can identify the exit in given direction relative to
     * the robot's current forward direction from the current position.
     * @return true if the exit of the maze is visible in a straight line of sight
     * @throws UnsupportedOperationException if robot has no sensor in this direction
     * Note, the above exception should not happen in any case for CS301 because we
     * will always have 4 sensors.
     *
     * given the maze dimensions, get the cell in the proper direction along the border
     * see if its the exit
     * return
     *
     * also, this costs one energies
     *
     * use the coordinate and the direction you're facing.
     * Then, utilize the directional sensors NICE and do that.
     *
     *
     * look ahead
     */
    @Override
    public boolean canSeeExit(Direction direction) throws UnsupportedOperationException {
        // this.depleteEnergy(1); not depleting it because the only place this is called is distance to obstacle

        boolean answer = false;

        switch(direction) {

            case FORWARD:
                return this.sensors[0].senseExit();

            case BACKWARD:
                return this.sensors[2].senseExit();

            case LEFT:
                return this.sensors[3].senseExit();

            case RIGHT:
                return this.sensors[1].senseExit();

        }

        return answer;
    }


    /**
     * Tells if current position is inside a room.
     * @return true if robot is inside a room, false otherwise
     * @throws UnsupportedOperationException if not supported by robot
     *
     * Uses the room sensor, which this robot has, so I don't really need to worry about this exception.
     */
    @Override
    public boolean isInsideRoom() throws UnsupportedOperationException {
        if(this.hasRoomSensor()) {
            return this.sensors[4].sense();
        }
        else {
            throw new UnsupportedOperationException("This Robot Cannot Sense Rooms");
        }
    }

    /**
     * Tells if the robot has a room sensor.
     *
     * i mean this is pretty easy just return if it has one or not
     */
    @Override
    public boolean hasRoomSensor() {
        return this.sensors[4] != null;
    }

    /**
     * Provides the current cardinal direction.
     *
     * @return cardinal direction is robot's current direction in absolute terms
     */
    @Override
    public CardinalDirection getCurrentDirection() {
        // TODO Auto-generated method stub
        this.cardinalFacing = this.getCardinalDirection();
        return this.cardinalFacing;
    }

    /**
     * Returns the current battery level.
     * The robot has a given battery level (energy level)
     * that it draws energy from during operations.
     * The particular energy consumption is device dependent such that a call
     * for distance2Obstacle may use less energy than a move forward operation.
     * If battery level <= 0 then robot stops to function and hasStopped() is true.
     * @return current battery level, level is > 0 if operational.
     */
    @Override
    public float getBatteryLevel() {
        // TODO Auto-generated method stub
        return this.batteryLevel;
    }

    /**
     * Takes float
     * sets battery level to that
     * yeah that's it.
     */
    @Override
    public void setBatteryLevel(float level) {
        // TODO Auto-generated method stub
        this.batteryLevel = level;
    }

    /**
     * Gets the distance traveled by the robot.
     * The robot has an odometer that calculates the distance the robot has moved.
     * Whenever the robot moves forward, the distance
     * that it moves is added to the odometer counter.
     * The odometer reading gives the path length if its setting is 0 at the start of the game.
     * @return the distance traveled measured in single-cell steps forward
     */
    @Override
    public int getOdometerReading() {
        return this.odometer;
    }

    /**
     * Resets odometer integer to 0.
     * very simple.
     */
    @Override
    public void resetOdometer() {
        this.odometer = 0;
    }

    /**
     * Gives the energy consumption for a full 360 degree rotation.
     * Scaling by other degrees approximates the corresponding consumption.
     * TODO: You can find this information in the project pdf
     * @return energy for a full rotation
     */
    @Override
    public float getEnergyForFullRotation() {
        return 12;
    }

    /**
     * Gives the energy consumption for moving forward for a distance of 1 step.
     * For simplicity, we assume that this equals the energy necessary
     * to move 1 step backwards and that scaling by a larger number of moves is
     * approximately the corresponding multiple.
     * @return energy for a single step forward
     *
     * TODO: this, again, is in the project pdf
     * will most likely be a constant number, but I suppose we can set it to a variable if that is easier
     */
    @Override
    public float getEnergyForStepForward() {
        return 5;
    }

    /**
     * Tells if the robot has stopped for reasons like lack of energy, hitting an obstacle, etc.
     *
     * The easy one here is energy. The harder part is identifying that the robot has hit a wall.
     * Other reasons, I suppose, could be related to exiting or actually finishing the game
     *
     * @return true if the robot has stopped, false otherwise
     *
     * implementation:
     * check energy -> return true if needed
     * check walls -> return true if needed
     * check game finished -> return true if needed
     * return false
     */
    @Override
    public boolean hasStopped() {
        // TODO Auto-generated method stub
        try {
            this.position = this.getCurrentPosition();
            this.cardinalFacing = this.getCardinalDirection();
            // hey we are stuck at a wall:
            if(this.hasWall(this.position[0], this.position[1], this.cardinalFacing)) {
                this.playState.checkGameOver(this.position[0], this.position[1], false);
                return true;
            }

            if(this.batteryLevel == 0) {
                this.playState.checkGameOver(this.position[0], this.position[1], false);
                return true;
            }

            this.playState.checkGameOver(this.position[0], this.position[1], false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Tells the distance to an obstacle (a wall or border)
     * in a direction as given and relative to the robot's current forward direction.
     * Distance is measured in the number of cells towards that obstacle,
     * e.g. 0 if current cell has a wall in this direction,
     * 1 if it is one step forward before directly facing a wall,
     * Integer.MaxValue if one looks through the exit into eternity.
     * @return number of steps towards obstacle if obstacle is visible
     * in a straight line of sight, Integer.MAX_VALUE otherwise
     * @throws UnsupportedOperationException if not supported by robot
     *
     * definitely check to see if the exit is in view first of this direction, first.
     * Then, you can use the nice cells.hasWall at different stages/directions.
     * Will need to consider the N,S,E,W directions, but that's okay.
     *
     * Also, this costs 1 energies
     */
    @Override
    public int distanceToObstacle(Direction direction) throws UnsupportedOperationException {
        if(!this.hasDistanceSensor(direction)) {
            return 0;
        }
        this.depleteEnergy(1);

        if(this.canSeeExit(direction)) {
            return Integer.MAX_VALUE;
        }

        switch(direction) {

            case FORWARD:
                return this.sensors[0].senseDirection();


            case BACKWARD:
                return this.sensors[2].senseDirection();


            case LEFT:
                return this.sensors[3].senseDirection();


            case RIGHT:
                return this.sensors[1].senseDirection();


        }

        return 0;
    }

    /**
     * Tells if the robot has a distance sensor for the given direction.
     * Since this interface is generic and may be implemented with robots
     * that are more or less equipped. The purpose is to allow for a flexible
     * robot driver to adapt its driving strategy according the features it
     * finds supported by a robot.
     *
     * This robot does have a distance sensor for all directions, so we will return true.
     */
    @Override
    public boolean hasDistanceSensor(Direction direction) {
        return true;
    }

    /**
     * Just uses the cardinal direction method from the controller to update the robot's compass
     */
    public CardinalDirection getCardinalDirection() {
        return this.playState.getCurrentDirection();
    }

    /**
     * used in rotate()
     * just makes it look nicer
     * @param turn - the type of turn (right, left, 180)
     * @return amount of energy needed for the given operation (which is a type of turn)
     */
    public float getEnergyForRotation(Turn turn) {
        switch(turn) {
            case LEFT:
                return 3;
            case RIGHT:
                return 3;
            case AROUND:
                return 6;
            default:
                return 0;
        }
    }

    /**
     * updates the position coordinates during a move.
     * only used with the move() function
     */
    public void updatePositionCoordinates() {
        this.position = this.playState.getCurrentPosition();
    }

    /**
     * sets playState boolean to a certain value
     */
    public void setInPlayingState(boolean bool){
        this.inPlayingState = bool;
    }

    /**
     * @return whether or not we are in a playing state.
     */
    public boolean isPlaying() {
        return this.playState.inPlayState();
    }


    /**
     * gets width from maze
     * @return maze width
     */
    @Override
    public int getWidth() {
        if(this.inPlayingState) {
            return this.playState.getWidth();
        }
        else{
            return 0;
        }
    }

    /**
     * @return maze height
     */
    @Override
    public int getHeight() {
        if(this.inPlayingState) {
          return this.playState.getHeight();
        }
        return 0;
    }

    /**
     * easy way to check if the energy is beneath any action that could be called
     * and to see if the game is over
     * @param amount is the amount of 'energy cells' that will be depleted from the energy
     */
    private void depleteEnergy(float amount) {
        float newAmount = this.getBatteryLevel() - amount;
        this.setBatteryLevel(newAmount);

        this.playState.checkGameOver(this.position[0], this.position[1], false);
    }

    /**
     * Used for the direction sensors to know which direction they are facing.
     * This is an issue because the sensors are on the left/right/front/back sides of a robot, but that's not really
     * that helpful when it comes to knowing which direction you're facing and which side you're checking.
     *
     * It's really an ugly method and is not that efficient, but it's Sunday morning and I want to do hiking so
     * I am doing this because it is easy. Maybe refactor it later.
     * @param s
     * @return
     */
    public CardinalDirection getSensorCardinalDirection(Sensor s) {
        if(s == sensors[0]) {
            return this.getCardinalDirection();
        }
        // this is for the right sensor
        else if(s == sensors[1]) {
            CardinalDirection forward = this.getCardinalDirection();
            if(forward == CardinalDirection.North) {
                return CardinalDirection.East;
            }
            else if(forward == CardinalDirection.East) {
                return CardinalDirection.South;
            }
            else if(forward == CardinalDirection.South) {
                return CardinalDirection.West;
            }
            else {
                return CardinalDirection.North;
            }

        }
        // this is for the back sensor
        // it is just going to return the opposite side
        else if(s == sensors[2]) {
            CardinalDirection forward = this.getCardinalDirection();
            if(forward == CardinalDirection.North) {
                return CardinalDirection.South;
            }
            else if(forward == CardinalDirection.East) {
                return CardinalDirection.West;
            }
            else if(forward == CardinalDirection.South) {
                return CardinalDirection.North;
            }
            else {
                return CardinalDirection.East;
            }
        }
        // this is for the left sensor
        else if(s == sensors[3]) {
            CardinalDirection forward = this.getCardinalDirection();
            if(forward == CardinalDirection.North) {
                return CardinalDirection.West;
            }
            else if(forward == CardinalDirection.East) {
                return CardinalDirection.North;
            }
            else if(forward == CardinalDirection.South) {
                return CardinalDirection.East;
            }
            else {
                return CardinalDirection.South;
            }
        }
        else {
            return this.getCardinalDirection();
        }

    }

    public void setSensors() {
        Sensor leftSensor = new DirectionSensor(this);
        Sensor rightSensor = new DirectionSensor(this);
        Sensor frontSensor = new DirectionSensor(this);
        Sensor backSensor = new DirectionSensor(this);

        Sensor roomSensor = new RoomSensor(this);

        sensors[0] = frontSensor;
        sensors[1] = rightSensor;
        sensors[2] = backSensor;
        sensors[3] = leftSensor;
        sensors[4] = roomSensor;
    }

    /**
     * This is a stupid method and it should not be here, but it works so here we are.
     * @param x coordinate
     * @param y coordinate
     * @param cd cardinal Direction in question
     * @return whether or not this cell has a wall
     */
    @Override
    public boolean hasWall(int x, int y, CardinalDirection cd) {
        if((x > this.getWidth() || y > this.getHeight()) || (x < 0 || y < 0)) {
            return false;
        }
        if(x<0 || y<0 || x> this.getWidth() || y>this.getHeight()) {
            return false;
        }
        return (this.mazeConfig.hasWall(x, y, cd));
    }

    @Override
    public boolean isExitPosition(int x, int y) {
        if((x > this.getWidth() || y > this.getHeight()) || (x < 0 || y < 0)) {
            return false;
        }
        return ((this.mazeConfig.getMazecells().isExitPosition(x, y)));
    }

    /**
     * If you're at the exit, rotate to view it.
     */
    @Override
    public void rotateToViewExit() {
        if(this.isAtExit()) {
            if(this.sensors[1].senseExit()) {
                this.depleteEnergy(1);
                this.rotate(Turn.LEFT);
            }
            else if(this.sensors[3].senseExit()) {
                this.depleteEnergy(1);
                this.rotate(Turn.RIGHT);
            }
        }
    }

    /**
     * Returns an array with which walls are bordering the robot.
     */
    @Override
    public boolean [] borderWalls() {
        boolean [] walls = new boolean [4];

        // front sensor
        walls[0] = this.sensors[0].sense();

        // right sensor
        walls[1] = this.sensors[3].sense();

        // back sensor
        walls[2] = this.sensors[2].sense();

        // left sensor
        walls[3] = this.sensors[1].sense();

        return walls;
    }

    @Override
    public void gameOverConfirmed() {
        this.playState.checkGameOver(this.position[0], this.position[1], true);
    }

    public MazeConfiguration getMazeConfig(){
        return mazeConfig;
    }
}
