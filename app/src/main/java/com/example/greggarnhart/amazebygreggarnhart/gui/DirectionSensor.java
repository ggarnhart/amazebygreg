package com.example.greggarnhart.amazebygreggarnhart.gui;

import generation.CardinalDirection;
import generation.MazeConfiguration;

public class DirectionSensor implements Sensor {

    /**
     * The robot the sensor is paired with.
     */
    Robot robbie;

    /**
     * The maze information that this sensor is going to use to learn things.
     * It's set whenever a robot is paired with the sensor.
     */
    MazeConfiguration mazeConfigs;

    /**
     * Instantiates a sensor
     */
    public DirectionSensor() {

    }

    /**
     * Instantiates a sensor and sets the robot to the sensor
     * @param r is the robot that will be paired with the sensor
     */
    public DirectionSensor(Robot r) {
        this.robbie = r;
        this.mazeConfigs = r.getMazeConfig();
    }

    @Override
    public void setRobot(Robot r) {
        this.robbie = r;
        this.mazeConfigs = r.getMazeConfig();
    }

    /**
     * Senses if a wall is right here in front of the sensor.
     * Uses Robbie's current position and the particular sensor to see if there is a wall in front of that sensor.
     *
     * Current Error: mazeConfigs is NULL !!! WTFFFF
     */
    @Override
    public boolean sense() {

        CardinalDirection cd = robbie.getSensorCardinalDirection(this);
        int[] pos = new int[2];
        try {
            pos = robbie.getCurrentPosition();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        boolean answer = this.mazeConfigs.hasWall(pos[0], pos[1], cd);
        return answer;

    }

    @Override
    public int senseDirection() {
        CardinalDirection cd = robbie.getSensorCardinalDirection(this);
        // System.out.println("Cardinal Direction: "+cd);
        int [] pos = new int [2];
        try {
            pos = robbie.getCurrentPosition();
        } catch(Exception e) {
            System.out.println("********************* Error in getting Current Position. See Trace Below. *********************");
            e.printStackTrace();
        }

        int distance = 0;
        boolean wallSensed = false;
        switch(cd) {
            case North:
                if(this.robbie.hasWall(pos[0], pos[1], cd)) {
                    return 0;
                }
                else{
                    int x = pos[0];
                    int y = pos[1];
                    while(y > 0 && !wallSensed) {
                        distance++;
                        if(this.robbie.hasWall(x, y, cd)) {
                            wallSensed = true;
                        }
                        y = y - 1;
                    }
                    return distance;
                }
            case East:
                if(this.robbie.hasWall(pos[0], pos[1], cd)) {
                    return 0;
                }
                else{
                    while(pos[0] < robbie.getWidth() && !wallSensed) {
                        distance++;
                        pos[0] = pos[0] + 1;
                        if(this.robbie.hasWall(pos[0], pos[1], cd)) {
                            wallSensed = true;
                        }
                    }
                    return distance;
                }
            case South:
                if(this.robbie.hasWall(pos[0], pos[1], cd)) {
                    return 0;
                }
                else{
                    while(pos[1] < robbie.getHeight() && !wallSensed) {
                        distance++;
                        pos[1] = pos[1] + 1;
                        if(this.robbie.hasWall(pos[0], pos[1], cd)) {
                            wallSensed = true;
                        }
                    }
                    return distance;
                }

            case West:
                if(this.robbie.hasWall(pos[0], pos[1], cd)) {
                    return 0;
                }
                else{
                    while(pos[0] >= 0 && !wallSensed) {
                        distance++;
                        pos[0] = pos[0] - 1;
                        if(this.robbie.hasWall(pos[0], pos[1], cd)) {
                            wallSensed = true;
                        }
                    }
                    return distance;
                }

        }

        return 0;

    }

    /**
     * A real doozy of a method here.
     *
     * First, it gets the exit coordinates and compares them with the current position to
     * see if it is even *possible* that the exit is on the same row/column.
     *
     * Then, it goes direction by direction to see if the sensor is facing the right direction to see the exit.
     * If it is facing the right direction, it goes cell by cell to see if there is a wall on any cell that may block
     * the exit.
     *
     * If there is a wall, if the sensor is facing the wrong direction, or some combination, it returns false.
     * Otherwise, you can see the exit and it returns true.
     *
     * @return whether or not it can "see" the exit
     */
    public boolean senseExit() {

        CardinalDirection cd = robbie.getSensorCardinalDirection(this);
        int[] pos = new int[2];
        try {
            pos = robbie.getCurrentPosition();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        boolean hitSomething = false;
        switch(cd) {
            case North:
                while(pos[1] < robbie.getHeight() && !hitSomething) {
                    if(this.robbie.hasWall(pos[0], pos[1], cd)){
                        return false;
                    }
                    else if(this.robbie.isExitPosition(pos[0], pos[1])) {
                        return true;
                    }
                    pos[1] = pos[1] + 1;
                }
                break;
            case East:
                while(pos[0] < robbie.getWidth() && !hitSomething) {
                    if(this.robbie.hasWall(pos[0], pos[1], cd)){
                        return false;
                    }
                    else if(this.robbie.isExitPosition(pos[0], pos[1])) {
                        return true;
                    }
                    pos[0] = pos[0] + 1;
                }
                break;
            case South:
                while(pos[1] >= 0 && !hitSomething) {
                    if(this.robbie.hasWall(pos[0], pos[1], cd)){
                        return false;
                    }
                    else if(this.robbie.isExitPosition(pos[0], pos[1])) {
                        return true;
                    }
                    pos[1] = pos[1] - 1;
                }
                break;
            case West:
                while(pos[0] >= 0 && !hitSomething) {
                    if(this.robbie.hasWall(pos[0], pos[1], cd)){
                        return false;
                    }
                    else if(this.robbie.isExitPosition(pos[0], pos[1])) {
                        return true;
                    }
                    pos[0] = pos[0] - 1;
                }
                break;
        }
        return false;
    }

}
