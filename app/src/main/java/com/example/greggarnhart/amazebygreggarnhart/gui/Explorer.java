package com.example.greggarnhart.amazebygreggarnhart.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import generation.CardinalDirection;
import generation.Cells;
import generation.MazeConfiguration;
import com.example.greggarnhart.amazebygreggarnhart.gui.Constants.UserInput;
import com.example.greggarnhart.amazebygreggarnhart.gui.Robot.Direction;
import com.example.greggarnhart.amazebygreggarnhart.gui.Robot.Turn;

/**
 * Wow here we are exploring, Lewis and Clark style.
 * Actually, it is really just Lewis tbh - if there are ever two robots, though,
 * Clark will join Lewis.
 *
 * Anyways, back to this.
 * The Explorer Robot is a poetic, liberal arts infused class.
 * It travels by 3 rules:
 *
 * 1. If outside of a room:
 * * - checks its options by asking its available distance sensors
 * * - picks a direction to the adjacent cell least traveled (ties are resolved by throwing the dice)
 * * - it rotates if necessary and moves a step into the chosen direction.
 *
 * 2. If the explorer gets into a room, it scans it to find all possible doors, picks the door least
 *    used (ties are resolved by randomization) and then runs for the door to leave the room.
 *
 * 3. Of course, if it is at the exit or it can see the exit, it goes for it.
 *
 * CRC Info:
 * Responsibilities
 * - Everything that all the other drivers do BUT
 *   it has a special drive2Exit function that abides by the above rules
 *
 * Relationships
 * - Same as before. Nice!
 * @author greg garnhart
 *
 */
/**
 * Wow here we are exploring, Lewis and Clark style.
 * Actually, it is really just Lewis tbh - if there are ever two robots, though,
 * Clark will join Lewis.
 *
 * Anyways, back to this.
 * The Explorer Robot is a poetic, liberal arts infused class.
 * It travels by 3 rules:
 *
 * 1. If outside of a room:
 * * - checks its options by asking its available distance sensors
 * * - picks a direction to the adjacent cell least traveled (ties are resolved by throwing the dice)
 * * - it rotates if necessary and moves a step into the chosen direction.
 *
 * 2. If the explorer gets into a room, it scans it to find all possible doors, picks the door least
 *    used (ties are resolved by randomization) and then runs for the door to leave the room.
 *
 * 3. Of course, if it is at the exit or it can see the exit, it goes for it.
 *
 * CRC Info:
 * Responsibilities
 * - Everything that all the other drivers do BUT
 *   it has a special drive2Exit function that abides by the above rules
 *
 * Relationships
 * - Same as before. Nice!
 * @author greg garnhart
 *
 */
public class Explorer implements RobotDriver {


    private Robot robbie;
    private int width = 0;
    private int height = 0;

    private boolean needsToMove = false;

    private float startingEnergy;

    private ArrayList<Integer> coords = new ArrayList<Integer>();

    /**
     * Keep track of where Lewis goes!
     */
    private HashMap<ArrayList<Integer>, Integer> travelLog;

    /**
     * Initialize a driver with a basic robot.
     * @param r is the robot the driver will be driving
     */
    public Explorer(Robot r) {
        this.robbie = r;
        this.setDimensions(this.robbie.getWidth(), this.robbie.getHeight());
        this.startingEnergy = this.robbie.getBatteryLevel();


        travelLog = new HashMap<ArrayList<Integer>, Integer>();
    }

    /**
     * Initialize Lewis without any information.
     */
    public Explorer() {
        // nothing to do here
        travelLog = new HashMap<ArrayList<Integer>, Integer>();
    }

    /**
     * Set a robot to Lewis and update needed information accordingly
     */
    @Override
    public void setRobot(Robot r) {
        this.robbie = r;
        this.setDimensions(this.robbie.getWidth(), this.robbie.getHeight());
        this.startingEnergy = this.robbie.getBatteryLevel();
    }


    @Override
    public void setDimensions(int width, int height) {
        this.width = width;
        this.height = height;
    }


    /**
     * Abides by the following rules:
     * * 1. If outside of a room:
     * * - checks its options by asking its available distance sensors
     * * - picks a direction to the adjacent cell least traveled (ties are resolved by throwing the dice)
     * * - it rotates if necessary and moves a step into the chosen direction.
     *
     * 2. If the explorer gets into a room, it scans it to find all possible doors, picks the door least
     *    used (ties are resolved by randomization) and then runs for the door to leave the room.
     *
     * 3. Of course, if it is at the exit or it can see the exit, it goes for it.
     *
     * For the sake of simplicity, the first two rules will be taken care of by other methods
     */
    @Override
    public boolean drive2Exit() throws Exception {
        boolean atExit = this.robbie.isAtExit();
        logExploration(); // log the starting spot
        if(!atExit && this.robbie.getBatteryLevel() > this.robbie.getEnergyForStepForward()) {

            boolean isInRoom = this.robbie.isInsideRoom();

            if(robbie.getBatteryLevel() <= this.robbie.getEnergyForStepForward()) {
                this.robbie.hasStopped();
                System.out.println("ALLDONE");
            }

            else if(needsToMove){
                robbie.move(1, false);
                needsToMove = false;
            }
            // easy money. drive to exit -- this can be called a few times tbh.
            else if(robbie.canSeeExit(Direction.FORWARD)) {
                robbie.move(1, false);
                logExploration();
            }
            else if(isInRoom) {
                this.leaveRoom();
            }
            else {
                this.explore();
            }
            // check if you're done!
            atExit = this.robbie.isAtExit();

        }
        return atExit;
    }

    /**
     * Method that documents Lewis' travels by writing
     * down how many times Lewis has traveled to a certain spot.
     */
    public void logExploration() throws Exception{
        int [] coordinates = robbie.getCurrentPosition();
        coords.add(coordinates[0]);
        coords.add(coordinates[1]);

        if(this.travelLog.containsKey(coords)){
            int cur = travelLog.get(coords);
            cur++;
            travelLog.put(coords, cur);
        }
        else{
            travelLog.put(coords, 1);
        }
    }

    /*
     * Note: many of the following methods do not need descriptions as they are self documenting.
     */

    @Override
    public float getEnergyConsumption() {
        return this.startingEnergy - this.robbie.getBatteryLevel();
    }

    @Override
    public int getPathLength() {
        return robbie.getOdometerReading();
    }

    @Override
    public void wakeUp() {
        System.out.println("Waking up the Lewis.");
        this.setDimensions(this.robbie.getWidth(), this.robbie.getHeight());

    }


    public boolean keyDown(UserInput key) {
        if(!robbie.isPlaying()) {
            return false;
        }
        switch(key) {
            case Up:
                System.out.println("Lewis is Driving towards the Exit");
                try {
                    if(!this.drive2Exit()) {
                        this.robbie.gameOverConfirmed();
                    }
                    else {
                        this.robbie.rotateToViewExit();
                        this.robbie.move(1, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
        return false;
    }

    /**
     * Method that is called when Lewis is in a room.
     * If the explorer gets into a room, it scans it to find all possible doors, picks the door least
     * used (ties are resolved by randomization) and then runs for the door to leave the room.
     *
     * Pseudo-code:
     * - scan all room cells (perhaps along the borders)
     * - - go along room border (in code - not in robot) and find doors
     * - Add all room cells (or doors) to the list
     * - randomly choose one
     * - drive to that door (and then through) that door.
     * - - go to the row the door is on, then go to the column. Not too hard tbh.
     * - that's all
     * @throws Exception for findDoors() method
     *
     */
    public void leaveRoom() throws Exception {
        logExploration(); // let's mark that first cell as visited
        ArrayList<ArrayList<Integer>> doors = findDoors();
        ArrayList<Integer> moveHere = whereToMove(doors);
        exitDoor(moveHere);
    }

    /**
     * Method that is called with Lewis is not in a room.
     * Most complicated - will use a few helper methods.
     *
     * Checks options by seeing which cells Lewis can travel to (uses the borderWalls() method)
     * Of those options, Lewis will travel to the one that has been visited the least. (That info is stored in
     * Lewis' travel log ((e.g. a hashmap)))
     *
     * After identifying which cell to travel to, Lewis will rotate to view that cell and move to it.
     *
     * That's it. The algorithm takes care of the rest.
     *
     * LET's GO.
     */
    public void explore() throws Exception{

        ArrayList<ArrayList<Integer>> moveOptions = this.availableCells();
        ArrayList<Integer> moveHere = this.whereToMove(moveOptions);
        // rotate to face this point
        rotateByCoordinate(moveHere);

        needsToMove = true;

        // move forward


        // log the move
        logExploration();

    }

    /**
     * Used in explore();
     * @return a list of coordinates that Lewis could move to
     * @throws Exception is used to appease the getCurrentPosition Method
     */
    private ArrayList<ArrayList<Integer>> availableCells() throws Exception{
        boolean [] walls = this.robbie.borderWalls();
        CardinalDirection cardinalDirectionBySide;
        ArrayList<ArrayList<Integer>> moveOptions = new ArrayList<ArrayList<Integer>>();
        for(int i = 0; i<4; i++){
            if(!walls[i]){
                switch(i){

                    // 0 is the front side
                    case 0:
                        cardinalDirectionBySide = cardinalDirectionOfSide(0);
                        ArrayList<Integer> forwards = getPointFromCardinalDirection(cardinalDirectionBySide);
                        moveOptions.add(forwards);
                        break;

                    // 1 is the right side
                    case 1:
                        cardinalDirectionBySide = cardinalDirectionOfSide(1);
                        ArrayList<Integer> right = getPointFromCardinalDirection(cardinalDirectionBySide);
                        moveOptions.add(right);
                        break;

                    // 2 is the back side
                    case 2:
                        cardinalDirectionBySide = cardinalDirectionOfSide(2);
                        ArrayList<Integer> behind = getPointFromCardinalDirection(cardinalDirectionBySide);
                        moveOptions.add(behind);
                        break;

                    // 3 is the left side
                    case 3:
                        cardinalDirectionBySide = cardinalDirectionOfSide(3);
                        ArrayList<Integer> left = getPointFromCardinalDirection(cardinalDirectionBySide);
                        moveOptions.add(left);
                        break;
                }
            }
        }

        return moveOptions;
    }

    /**
     * Given moveOptions, choose which cell to move to next.
     * In the event of a tie, choose randomly.
     * @param moveOptions is the possible locations
     * @return a point to move to.
     */
    private ArrayList<Integer> whereToMove(ArrayList<ArrayList<Integer>>  moveOptions){
        if(moveOptions.size() == 1) {
            return moveOptions.get(0);
        }

        Random rand = new Random();

        ArrayList<Integer> minFound = new ArrayList<Integer>();
        minFound = moveOptions.get(0);
        int min = Integer.MAX_VALUE;
        for(ArrayList<Integer> currentPoint : moveOptions){
            if(this.travelLog.containsKey(currentPoint)){
                int temp = travelLog.get(currentPoint);
                if(min > temp){
                    min = temp;
                    minFound = currentPoint;
                }
            }
            else if(min == 0 && rand.nextBoolean()){
                minFound = currentPoint;
            }
            else{
                min = 0;
                minFound = currentPoint;
            }
        }

        return minFound;
    }

    /**
     * given a coordinate, rotate Lewis to view that Coordinate
     * @param moveHere is the coordinate Lewis needs to face
     * @throws Exception is used to appease the getCurrentPosition Method
     */
    private void rotateByCoordinate(ArrayList<Integer> moveHere) throws Exception{
        CardinalDirection cd = pointDirection(moveHere);
        // System.out.println("The point is to the "+ cd );
        CardinalDirection cur = robbie.getCurrentDirection();

        if(cd != cur){
            switch(cur){
                case North:
                    if(cd == CardinalDirection.South){
                        robbie.rotate(Turn.AROUND);
                    }
                    else if(cd == CardinalDirection.West){
                        robbie.rotate(Turn.RIGHT); // i think these were programmed backwards, so ya
                    }
                    else{
                        robbie.rotate(Turn.LEFT);
                    }
                    break;

                case East:
                    if(cd == CardinalDirection.West){
                        robbie.rotate(Turn.AROUND);
                    }
                    else if(cd == CardinalDirection.North){
                        robbie.rotate(Turn.RIGHT); // i think these were programmed backwards, so ya
                    }
                    else{
                        robbie.rotate(Turn.LEFT);
                    }
                    break;

                case South:
                    if(cd == CardinalDirection.North){
                        robbie.rotate(Turn.AROUND);
                    }
                    else if(cd == CardinalDirection.West){
                        robbie.rotate(Turn.LEFT); // i think these were programmed backwards, so ya
                    }
                    else{
                        robbie.rotate(Turn.RIGHT);
                    }
                    break;

                case West:
                    if(cd == CardinalDirection.East){
                        robbie.rotate(Turn.AROUND);
                    }
                    else if(cd == CardinalDirection.North){
                        robbie.rotate(Turn.LEFT); // i think these were programmed backwards, so ya
                    }
                    else{
                        robbie.rotate(Turn.RIGHT);
                    }
                    break;
            }
        }
    }

    /**
     * Determines which direction the next point is in
     * @param moveHere is the next point
     * @return the Cardinal Direction of where Lewis will be traveling to next.
     * @throws Exception is there to appease the getCurrentPosition Method
     */
    private CardinalDirection pointDirection(ArrayList<Integer> moveHere) throws Exception{
        int [] current = robbie.getCurrentPosition();

        // to the EAST
        if(current[0] < moveHere.get(0)){
            return CardinalDirection.East;
        }
        // to the NORTH
        else if(current[1] > moveHere.get(1)){
            return CardinalDirection.North;
        }
        // to the South
        else if(current[1] < moveHere.get(1)){
            return CardinalDirection.South;
        }
        // to the West
        else if(current[0] > moveHere.get(0)){
            return CardinalDirection.West;
        }

        return this.robbie.getCurrentDirection();
    }

    /**
     * Gives the cardinal direction of a side of Robbie. (e.g. the left side may be north)
     * sending in a 0 --> forwards
     * sending in a 1 --> right
     * sending in a 2 --> behind
     * sending in a 3 --> left
     * @return cd of the side
     */
    private CardinalDirection cardinalDirectionOfSide(int side) {
        CardinalDirection ans = robbie.getCurrentDirection();
        CardinalDirection curDir = robbie.getCurrentDirection();
        switch(side) {
            case 0:
                // if it is 0, it's the front side, aka the direction you're already facing.
                return ans;
            case 1:
                if(curDir == CardinalDirection.North) {
                    return CardinalDirection.West;
                }
                else if(curDir == CardinalDirection.East) {
                    return CardinalDirection.North;
                }
                else if(curDir == CardinalDirection.South) {
                    return CardinalDirection.East;
                }
                else if(curDir == CardinalDirection.West) {
                    return CardinalDirection.South;
                }
                break;
            case 2:
                if(curDir == CardinalDirection.North) {
                    return CardinalDirection.South;
                }
                else if(curDir == CardinalDirection.East) {
                    return CardinalDirection.West;
                }
                else if(curDir == CardinalDirection.South) {
                    return CardinalDirection.North;
                }
                else if(curDir == CardinalDirection.West) {
                    return CardinalDirection.East;
                }
                break;
            case 3:
                if(curDir == CardinalDirection.North) {
                    return CardinalDirection.East;
                }
                else if(curDir == CardinalDirection.East) {
                    return CardinalDirection.South;
                }
                else if(curDir == CardinalDirection.South) {
                    return CardinalDirection.West;
                }
                else if(curDir == CardinalDirection.West) {
                    return CardinalDirection.North;
                }
                break;
            default:
                break;
        }
        return ans;
    }

    /**
     *
     * @param dir is the direction of the point (e.g. the point is to the east!)
     * @throws Exception for current position
     */
    private ArrayList<Integer> getPointFromCardinalDirection(CardinalDirection dir) throws Exception {
        ArrayList<Integer> answer = new ArrayList<Integer>();
        int [] curPos = this.robbie.getCurrentPosition();
        switch(dir) {
            case North:
                answer.add(curPos[0]);
                answer.add(curPos[1]-1);
                break;
            case East:
                answer.add(curPos[0]+1);
                answer.add(curPos[1]);
                break;
            case South:
                answer.add(curPos[0]);
                answer.add(curPos[1]+1);
                break;
            case West:
                answer.add(curPos[0]-1);
                answer.add(curPos[1]);
                break;
        }

        return answer;
    }

    /**
     * goes through the room programmatically and finds the doors.
     * adds the current position (which will be added because it is the first entered cell in the room, so robbie has to be near a door.
     * goes along the left wall to find the other doors :)
     * -- while there is no front wall add all doors
     * -- when there is a front wall, turn left
     * -- when you are back to the starting position, stop!
     * @return the list of doors. Duh.
     * @throws Exception for current position errors
     */
    private ArrayList<ArrayList<Integer>> findDoors() throws Exception{
        ArrayList<ArrayList<Integer>> doors = new ArrayList<ArrayList<Integer>>();

        // add the current positions (bc u are at a door rn!!)
        int [] curPos = robbie.getCurrentPosition();
        ArrayList<Integer> temp = new ArrayList<Integer>();
        temp.add(curPos[0]);
        temp.add(curPos[1]);
        doors.add(temp);

        // now, go through and find the doors :)
        doorsBySide(CardinalDirection.North, doors);
        doorsBySide(CardinalDirection.East, doors);
        doorsBySide(CardinalDirection.South, doors);
        doorsBySide(CardinalDirection.West, doors);


        return doors;

    }

    private void doorsBySide(CardinalDirection cd, ArrayList<ArrayList<Integer>> masterList) throws Exception{
        ArrayList<ArrayList<Integer>> answer = new ArrayList<ArrayList<Integer>>();
        /*
         * For each of these, start out by finding a starting position on that wall.
         * for north, subtract from the y until there is a wall to the north.
         * then move to the east and west until there is a wall to the east and west :)
         */


        MazeConfiguration mazeConfigs = robbie.getMazeConfig();
        Cells cells = mazeConfigs.getMazecells();

        int [] curPos = this.robbie.getCurrentPosition();
        int xCoordinate = curPos[0];
        int yCoordinate = curPos[1];
        int xCopy = xCoordinate;
        int yCopy = yCoordinate;

        switch(cd) {
            case North:
                while(!cells.hasWall(xCoordinate,yCoordinate,CardinalDirection.North) && cells.isInRoom(xCoordinate, yCoordinate)) {
                    yCoordinate = yCoordinate - 1;
                }

                // check to see if we went too far ;)
                if(!cells.isInRoom(xCoordinate,yCoordinate)) {
                    yCoordinate = yCoordinate+1;
                }


                xCopy = xCoordinate;
                while(cells.isInRoom(xCopy, yCoordinate)) {
                    if(cells.hasNoWall(xCopy, yCoordinate, CardinalDirection.North)){
                        ArrayList<Integer> tempList = new ArrayList<Integer>();
                        tempList.add(xCopy);
                        tempList.add(yCoordinate);
                        if(!answer.contains(tempList) && !(xCopy == curPos[0] && yCopy == curPos[1] )) {
                            answer.add(tempList);
                        }
                    }
                    xCopy = xCopy+1;
                }

                xCopy = xCoordinate;
                while(cells.isInRoom(xCopy, yCoordinate)) {
                    if(cells.hasNoWall(xCopy, yCoordinate, CardinalDirection.North)){
                        ArrayList<Integer> tempList = new ArrayList<Integer>();
                        tempList.add(xCopy);
                        tempList.add(yCoordinate);
                        if(!answer.contains(tempList) && !(xCopy == curPos[0] && yCopy == curPos[1])) {
                            answer.add(tempList);
                        }
                    }
                    xCopy = xCopy-1;
                }
                break;

            case East:
                while(!cells.hasWall(xCoordinate,yCoordinate,CardinalDirection.East) && cells.isInRoom(xCoordinate, yCoordinate)) {
                    xCoordinate = xCoordinate + 1;
                }

                // check to see if we went too far ;)
                if(!cells.isInRoom(xCoordinate,yCoordinate)) {
                    xCoordinate = xCoordinate-1;
                }

                yCopy = yCoordinate;

                while(cells.isInRoom(xCoordinate, yCopy)) {
                    if(cells.hasNoWall(xCoordinate, yCopy, CardinalDirection.East)){
                        ArrayList<Integer> tempList = new ArrayList<Integer>();
                        tempList.add(xCoordinate);
                        tempList.add(yCopy);
                        if(!answer.contains(tempList) && !(xCopy == curPos[0] && yCopy == curPos[1])) {
                            answer.add(tempList);
                        }
                    }
                    yCopy = yCopy-1;
                }

                yCopy = yCoordinate;

                while(cells.isInRoom(xCoordinate, yCopy)) {
                    if(cells.hasNoWall(xCoordinate, yCopy, CardinalDirection.East)){
                        ArrayList<Integer> tempList = new ArrayList<Integer>();
                        tempList.add(xCoordinate);
                        tempList.add(yCopy);
                        if(!answer.contains(tempList) && !(xCopy == curPos[0] && yCopy == curPos[1])) {
                            answer.add(tempList);
                        }
                    }
                    yCopy = yCopy+1;
                }

                break;

            case South:
                while(!cells.hasWall(xCoordinate,yCoordinate,CardinalDirection.South) && cells.isInRoom(xCoordinate, yCoordinate)) {
                    yCoordinate = yCoordinate + 1;
                }

                // check to see if we went too far ;)
                if(!cells.isInRoom(xCoordinate,yCoordinate)) {
                    yCoordinate = yCoordinate-1;
                }

                xCopy = xCoordinate;

                while(cells.isInRoom(xCopy, yCoordinate)) {
                    if(cells.hasNoWall(xCopy, yCoordinate, CardinalDirection.South)){
                        ArrayList<Integer> tempList = new ArrayList<Integer>();
                        tempList.add(xCopy);
                        tempList.add(yCoordinate);
                        if(!answer.contains(tempList) && !(xCopy == curPos[0] && yCopy == curPos[1])) {
                            answer.add(tempList);
                        }
                    }
                    xCopy = xCopy+1;
                }

                xCopy = xCoordinate;
                while(cells.isInRoom(xCopy, yCoordinate)) {
                    if(cells.hasNoWall(xCopy, yCoordinate, CardinalDirection.South)){
                        ArrayList<Integer> tempList = new ArrayList<Integer>();
                        tempList.add(xCopy);
                        tempList.add(yCoordinate);
                        if(!answer.contains(tempList) && !(xCopy == curPos[0] && yCopy == curPos[1])) {
                            answer.add(tempList);
                        }
                    }
                    xCopy = xCopy-1;
                }
                break;


            case West:
                while(!cells.hasWall(xCoordinate,yCoordinate,CardinalDirection.West) && cells.isInRoom(xCoordinate, yCoordinate)) {
                    xCoordinate = xCoordinate - 1;
                }

                // check to see if we went too far ;)
                if(!cells.isInRoom(xCoordinate,yCoordinate)) {
                    xCoordinate = xCoordinate+1;
                }

                yCopy = yCoordinate;

                while(cells.isInRoom(xCoordinate, yCopy)) {
                    if(cells.hasNoWall(xCoordinate, yCopy, CardinalDirection.West)){
                        ArrayList<Integer> tempList = new ArrayList<Integer>();
                        tempList.add(xCoordinate);
                        tempList.add(yCopy);
                        if(!answer.contains(tempList) && !(xCopy == curPos[0] && yCopy == curPos[1])) {
                            answer.add(tempList);
                        }
                    }
                    yCopy = yCopy-1;
                }

                yCopy = yCoordinate;
                while(cells.isInRoom(xCoordinate, yCopy)) {
                    if(cells.hasNoWall(xCoordinate, yCopy, CardinalDirection.West)){
                        ArrayList<Integer> tempList = new ArrayList<Integer>();
                        tempList.add(xCoordinate);
                        tempList.add(yCopy);
                        if(!answer.contains(tempList) && !(xCopy == curPos[0] && yCopy == curPos[1])) {
                            answer.add(tempList);
                        }
                    }
                    yCopy = yCopy+1;
                }


                break;
        }
        for(ArrayList<Integer> list : answer) {
            if(!masterList.contains(answer)) {
                masterList.add(list);
            }
        }

    }

    /**
     * We have a door coordinate, but now we need to drive to it! Let's GO.
     * First, move north or south, if necessary.
     * @param moveHere is the coordinate that we will be driving to
     * @throws Exception is thrown when getCurrentPosition fails
     */
    private void exitDoor(ArrayList<Integer> moveHere) throws Exception {
        int [] currentPosition = this.robbie.getCurrentPosition();
        int curX = currentPosition[0];
        int curY = currentPosition[1];

        if(curY < moveHere.get(1) || curY > moveHere.get(1)) {
            ArrayList<Integer> rotateToThis = new ArrayList<Integer>();
            rotateToThis.add(curX);
            rotateToThis.add(moveHere.get(1));
            rotateByCoordinate(rotateToThis);

            if(curY < moveHere.get(1)) {
                robbie.move(moveHere.get(1) - curY, false);
            }
            else {
                robbie.move(curY - moveHere.get(1), false);
            }
        }

        if(curX < moveHere.get(0) || curX > moveHere.get(0)) {
            ArrayList<Integer> rotateToThis = new ArrayList<Integer>();
            rotateToThis.add(moveHere.get(0));
            rotateToThis.add(moveHere.get(1));
            rotateByCoordinate(rotateToThis);

            if(curX < moveHere.get(0)) {
                robbie.move(moveHere.get(0) - curX, false);
            }
            else {
                robbie.move(curX - moveHere.get(0), false);
            }
        }

        walkThroughDoor();


    }

    /**
     * Lewis is now at a cell with a door, but how do we know which side to go through?
     * Great news. walkThroughDoor is all all about fixing that.
     * @throws Exception
     */
    private void walkThroughDoor() throws Exception {
        int [] curPos = this.robbie.getCurrentPosition();
        boolean [] possibilities = new boolean[4];


        MazeConfiguration mazeConfigs = robbie.getMazeConfig();
        Cells cells = mazeConfigs.getMazecells();

        int x = curPos[0];
        int y = curPos[1];

        HashMap<CardinalDirection, Boolean> whichDoors = new HashMap<CardinalDirection, Boolean>();

        // north first
        if(cells.hasNoWall(x, y, CardinalDirection.North) && !cells.isInRoom(x, y-1)) {
            possibilities[0] = true;
            whichDoors.put(CardinalDirection.North, true);
        }

        // east
        if(cells.hasNoWall(x, y, CardinalDirection.East) && !cells.isInRoom(x+1, y)) {
            possibilities[1] = true;
            whichDoors.put(CardinalDirection.East, true);
        }

        // south
        if(cells.hasNoWall(x, y, CardinalDirection.South) && !cells.isInRoom(x, y+1)) {
            possibilities[2] = true;
            whichDoors.put(CardinalDirection.South, true);
        }

        // west
        if(cells.hasNoWall(x, y, CardinalDirection.West) && !cells.isInRoom(x-1, y)) {
            possibilities[3] = true;
            whichDoors.put(CardinalDirection.West, true);
        }

        // go through them!
        Random rand = new Random();
        int chosen = -1;
        CardinalDirection chosenDirection = this.robbie.getCurrentDirection();

        for(Map.Entry<CardinalDirection, Boolean> entry : whichDoors.entrySet()) {
            if(entry.getValue() && chosen == -1) {
                chosenDirection = entry.getKey();
                chosen = 0;
            }
            else if(entry.getValue() && rand.nextBoolean()) {
                chosenDirection = entry.getKey();
            }
        }

        rotateByCoordinate(getPointFromCardinalDirection(chosenDirection));
        robbie.move(1, false);
        logExploration();
    }

}
