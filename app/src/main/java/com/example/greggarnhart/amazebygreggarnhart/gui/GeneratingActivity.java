package com.example.greggarnhart.amazebygreggarnhart.gui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.greggarnhart.amazebygreggarnhart.R;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import generation.GlobalClass;
import generation.MazeConfiguration;
import generation.MazeContainer;
import generation.MazeFactory;
import generation.Order;


public class GeneratingActivity extends AppCompatActivity {

    // Extra Strings
    public static final String DRIVER = "com.example.greggarnhart.amazedbygregarnhart.RobotDriver";


    private ProgressBar progressBar;
    private int progress;
    private Handler generatorHandler;
    private TextView generatingLabel;
    private String driver;
    private String filename;
    private Order.Builder algo;

    private MazeFactory mazeFactory;
    private int seed = 0;

    private boolean needToSetSeed = false;


    /**
     * This is of particular importance because of the use of Handler.
     * Um yes we generate the maze in the background, so yes WATCH OUT.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_build_maze);

        GlobalClass nightModeQuestionMark = (GlobalClass) getApplicationContext();
        if(nightModeQuestionMark.nightMode){
            nightMode();
        }

        getSeeds();

        Intent intent = getIntent();
        String levelNumber = "0";
        levelNumber = intent.getStringExtra(AMazeActivity.CHOSEN_LEVEL);
        String algorithm = "DFS";
        algorithm = intent.getStringExtra(AMazeActivity.CHOSEN_ALGORITHM);
        driver = intent.getStringExtra(AMazeActivity.CHOSEN_DRIVER);

        String bad = intent.getStringExtra(AMazeActivity.REVISIT_BOOL);
        if(bad.equals("true")){
            needToSetSeed = true;
        }
        else{
            needToSetSeed = false;
        }

        generatingLabel = findViewById(R.id.generatingMazeLabel);
        generatingLabel.setText("Generating Maze with the " + algorithm + " algorithm.");

        TextView levelLabel = (TextView) findViewById(R.id.levelLabel);
        levelLabel.setText("Level " + levelNumber + " Maze.");
        int level = Integer.parseInt(levelNumber);

        algo = findBuilder(algorithm);

        progressBar = (ProgressBar) findViewById(R.id.mazeGenerationBar);
        progress = 10;

        Context context = getApplicationContext();
        CharSequence text = "Generating maze with " + algorithm + "!";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        if(!needToSetSeed){
            toast.show();
        }
        else{
            Toast fileToast = Toast.makeText(context, "revisiting maze", duration);
            fileToast.show();
        }


        Thread backgroundThread = new Thread(new BackgroundBuild(level, algo));
        backgroundThread.start();

        // USED to update the progress bar
        generatorHandler = new Handler() {
            @Override
            public void handleMessage(Message message) {

                progress = message.arg1;
                progressBar.setProgress(progress);
                if (progress == 100 || progress > 100) {
                    moveToPlaying();
                }

            }
        };
    }

    private void getSeeds() {
        Log.v("Seeds Retrieval", "Getting the seeds");
        String zeroSeed = "";

        FileInputStream fileInputStream = null;
        try {
            fileInputStream = openFileInput("zeroMaze");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            zeroSeed = bufferedReader.readLine();
            Log.v("Seed Retrieved!", zeroSeed);
            Long temp = Long.parseLong(zeroSeed);
            temp = temp % 100000;
            seed = Math.toIntExact(temp);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Move to Playing switches activities upon the completion of a maze generation!
     *
     * @param view
     */
    public void moveToPlaying(View view) {
        int progress = progressBar.getProgress();



        if (progress < 100) {
            Context context = getApplicationContext();
            CharSequence text = "Generation needs to finish!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        } else {
            // play manually
            if (driver.equals("Manual")) {

                Intent intent = new Intent(this, PlayManually.class);
                // start the maze portion!
                startActivity(intent);
            }

            // play not manually
            else {
                Intent intent = new Intent(this, PlayAnimationActivity.class);
                Intent mainActivity = getIntent();
                // start the maze portion!
                intent.putExtra(DRIVER, mainActivity.getStringExtra(AMazeActivity.CHOSEN_DRIVER));
                startActivity(intent);
            }
        }
        Log.v("Enter Maze Button", "Enter the Maze!");
    }

    public void moveToPlaying() {
        int progress = progressBar.getProgress();

        if (progress < 100) {
            Context context = getApplicationContext();
            CharSequence text = "Generation needs to finish!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        } else {
            // play manually
            if (driver.equals("Manual")) {

                Intent intent = new Intent(this, PlayManually.class);
                // start the maze portion!
                startActivity(intent);
            }

            // play not manually
            else {
                Intent intent = new Intent(this, PlayAnimationActivity.class);
                Intent mainActivity = getIntent();
                // start the maze portion!
                intent.putExtra(DRIVER, mainActivity.getStringExtra(AMazeActivity.CHOSEN_DRIVER));
                startActivity(intent);
            }
        }
        Log.v("Enter Maze Button", "Enter the Maze!");
    }

    /**
     * Quick method that returns the correct type of builder.
     *
     * @param builderLabel
     * @return
     */
    private Order.Builder findBuilder(String builderLabel) {
        switch (builderLabel) {
            case "DFS":
                return Order.Builder.DFS;
            case "Ellers":
                return Order.Builder.Eller;
            case "Prim":
                return Order.Builder.Prim;
            default:
                return Order.Builder.DFS;
        }
    }

    /**
     * Hm let's put this here instead. Seems easier this way;)
     */
    class BackgroundBuild implements Runnable, Order {

        private int level;
        private Builder algorithm;

        BackgroundBuild(int level, Order.Builder algorithm) {
            this.level = level;
            this.algorithm = algorithm;
        }

        @Override
        public int getSkillLevel() {
            return level;
        }

        @Override
        public Builder getBuilder() {
            return this.algorithm;
        }

        // TODO: Determine whether or not this even needs to be fixed
        @Override
        public boolean isPerfect() {
            return false;
        }

        /**
         * Assings the generated maze to the global variable
         * @param  mazeConfig is the generated maze
         */
        @Override
        public void deliver(MazeConfiguration mazeConfig) {
            GlobalClass gc = (GlobalClass) getApplicationContext();
            gc.mazeConfiguration = mazeConfig; // set this as a global variable
        }

        /**
         * HEY we can use the handler above to do this :)
         *
         * @param percentage of job completion
         */
        @Override
        public void updateProgress(int percentage) {
            Message message = Message.obtain();
            message.arg1 = percentage;
            generatorHandler.sendMessage(message);
        }

        @Override
        public MazeContainer getMazeContainer() {
            return null;
        }

        @Override
        public void run() {
            // Um let's do the maze factory, shall we? ;)
            // these are the vars initialized in eclipse
            mazeFactory = new MazeFactory();
            if(needToSetSeed){
                mazeFactory.setSeed(seed);
            }
            mazeFactory.order(this);
        }
    }

    public void nightMode(){
        int deepBlue = Color.parseColor("#00303D");
        getWindow().getDecorView().setBackgroundColor(deepBlue);

        String fontColor = "#F3F3F3";
        String buttonColor = "#37CFCA";

        int fontColorInt = Color.parseColor(fontColor);

        TextView levelLabel = (TextView) findViewById(R.id.levelLabel);
        generatingLabel = findViewById(R.id.generatingMazeLabel);

        levelLabel.setTextColor(fontColorInt);
        generatingLabel.setTextColor(fontColorInt);

    }
}
