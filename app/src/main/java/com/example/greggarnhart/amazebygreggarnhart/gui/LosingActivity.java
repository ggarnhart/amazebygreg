package com.example.greggarnhart.amazebygreggarnhart.gui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.greggarnhart.amazebygreggarnhart.R;

public class LosingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.v("Losing","HEY we are losing now.");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_losing);

        Intent past = getIntent();
        Bundle extras = past.getExtras();
        Boolean didUseRobot = extras.getBoolean(PlayAnimationActivity.ROBOT_USED);

        if(didUseRobot){
            TextView energyConsumptionLabel = findViewById(R.id.energyConsumptionLabel);
            String amountUsed = past.getStringExtra(PlayAnimationActivity.CELLS_CONSUMED);
            energyConsumptionLabel.setText("Your driver used "+amountUsed+" energy cells and didn't exit the maze. Sorry!");

            TextView stepsLabel =  findViewById(R.id.stepsLabel);
            String stepsUsed = past.getStringExtra(PlayAnimationActivity.STEPS_TAKEN);
            String stepsNeeded = past.getStringExtra(PlayAnimationActivity.SHORTEST_PATH);

            stepsLabel.setText("Your driver moved "+stepsUsed+" steps before running out of energy. If your driver were to run a marathon, they'd probably stop after a 5k. Whoops!");

        }
        else{
            TextView energyConsumptionLabel = findViewById(R.id.energyConsumptionLabel);
            energyConsumptionLabel.setVisibility(View.INVISIBLE);

            TextView stepsLabel =  findViewById(R.id.stepsLabel);
            String stepsUsed = past.getStringExtra(PlayManually.STEPS_TAKEN);
            String stepsNeeded = past.getStringExtra(PlayManually.SHORTEST_PATH);

            stepsLabel.setText("You moved "+stepsUsed+" steps before running out of energy. You could have made it in "+stepsNeeded+".");


        }
    }

    public void backToMenu(View view){
        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);

        Log.v("Back to Menu Button", "Button Tapped, moving to menu!");
    }
}
