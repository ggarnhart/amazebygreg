package com.example.greggarnhart.amazebygreggarnhart.gui;

import com.example.greggarnhart.amazebygreggarnhart.gui.Robot.Turn;

/**
 *
 * @author Greg Garnhart
 * Manual driver takes keyboard input and allows the user to control the Basic Robot.
 * It results in basically the same play funtionality as the Project 2, but now
 * users have to worry about energy levels too.
 *
 * The RobotDriver interface has a few pretty detailed methods, but a lot are not important for this project.
 * Because of that, the methods this particular class implements are listed below:
 * - setRobot (self explanatory. MazeAppication calls this)
 * - setDimensions (sets maze dimensions to instance variables. Interacts with Controller class for this)
 * - getEnergyConsumption (allows for that added level of game play to be implemented. Interacts with BasicRobot.java for this)
 * - getPathLength (allows for the other added level of game play to be implemented. Interacts with BasicRobot.java)
 *
 * Class Relationships
 * - Is instantiated by MazeApplication
 * - Works in conjunction with BasicRobot
 * - Works with Controller to get information it needs (especially for different play states)
 *
 */

public class ManualDriver implements RobotDriver {
    private Robot robbie;
    private int width = 0;
    private int height = 0;

    private float startingEnergy;


    public ManualDriver() {
        // robot is set by a different method.
    }

    /**
     * this is called when the play state starts so we can update everything.
     */
    @Override
    public void wakeUp() {
        System.out.println("Waking up Robbie.");
        this.setDimensions(this.robbie.getWidth(), this.robbie.getHeight());

    }

    /**
     * called by MazeApplication to send in whatever robot needs to be sent in
     * and assigns it to the driver.
     */
    @Override
    public void setRobot(Robot r) {
        // TODO Auto-generated method stub
        this.robbie = r;
        this.startingEnergy = this.robbie.getBatteryLevel();
    }

    /**
     * Provides the robot driver with information on the dimensions of the 2D maze
     * measured in the number of cells in each direction.
     * @param width of the maze
     * @param height of the maze
     * @precondition 0 <= width, 0 <= height of the maze.
     */
    @Override
    public void setDimensions(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Also not used for this driver. This is for future Greg.
     */
    @Override
    public boolean drive2Exit() throws Exception {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     * Used as a measure of efficiency.
     * Takes the Robot's starting energy and subtracts it from the current energy and returns that answer.
     *
     * @return is the starting energy - the current energy.
     */
    @Override
    public float getEnergyConsumption() {
        return this.startingEnergy - this.robbie.getBatteryLevel();
    }

    /**
     * Returns the total length of the journey in number of cells traversed.
     * Being at the initial position counts as 0.
     * This is used as a measure of efficiency for a robot driver.
     *
     * returns Robot's odometer value.
     */
    @Override
    public int getPathLength() {
        return robbie.getOdometerReading();
    }


    /////////////////////////// Manual Methods Only ///////////////////////////
//    /**
//     *
//     * @param key -- which feature the user selected (in this case, probably *only* the arrow keys. Everything else can be done as normal
//     * @return false if the state hasn't started yet. Otherwise, it is true.
//     */
//    @Override
//    public boolean keyDown(UserInput key) {
//        if(!robbie.isPlaying()) {
//            return false;
//        }
//        switch(key) {
//            case Up:
//                this.robbie.move(1, true);
//                System.out.println("moving robbie forward. beep beep");
//                break;
//
//            case Left:
//                this.robbie.rotate(Turn.LEFT);
//                System.out.println("rotating robbie to the left. beep beep");
//                break;
//
//            case Right:
//                this.robbie.rotate(Turn.RIGHT);
//                System.out.println("rotating robbie to the right. beep beep");
//                break;
//            case Down:
//                this.robbie.rotate(Turn.AROUND);
//                System.out.println("rotating robbie around. beep beep");
//                break;
//            default:
//                System.out.println("robbie does not support that type of input. beep boop");
//                break;
//        }
//        return true;
//    }
}
