package com.example.greggarnhart.amazebygreggarnhart.gui;


import generation.CardinalDirection;
import generation.Cells;
import generation.MazeConfiguration;



/**
 * This class encapsulates all functionality to draw a map of the overall maze,
 * the set of visible walls, the solution.
 * The map is drawn on the screen in such a way that the current position
 * remains at the center of the screen.
 * The current position is visualized as a red dot with an attached arc
 * for its current direction.
 * The solution is visualized as a yellow line from the current position
 * towards the exit of the map.
 * Walls that have been visible in the first person view are drawn white,
 * all other walls that were never shown before are drawn in grey.
 * It is possible to zoom in and out of the map by increasing or decreasing
 * the map scale.
 *
 * This code is refactored code from Maze.java by Paul Falstad,
 * www.falstad.com, Copyright (C) 1998, all rights reserved
 * Paul Falstad granted permission to modify and use code for teaching purposes.
 * Refactored by Peter Kemper
 *
 * Updated to be refactores without awt.graphics. Stay tuned for Color
 */
public class MapDrawer {

    // keep local copies of values determined for UI appearance
    final int viewWidth;  // set to Constants.VIEW_WIDTH,
    final int viewHeight; // set to Constants.VIEW_HEIGHT
    final int mapUnit;    // set to Constants.MAP_UNIT
    final int stepSize;  // set to Constants.STEP_SIZE, typical value: map_unit/4

    /**
     * The user can increment or decrement the scale of the map.
     * map_scale is used to keep track of the current setting.
     * Minimum value is 1.
     */
    int mapScale;

    /**
     * SeenCells contains information on walls that are seen from the current point of view.
     * The field is set by the constructor. The referenced object is shared with
     * the FirstPersonDrawer that writes content into it. The MapDrawer only
     * reads content to decide which lines to draw and in which color.
     */
    final Cells seenCells ;

    /**
     * Contains all necessary information about current maze, i.e.
     * cells: location of walls
     * dists: distance to exit
     * width and height of the maze
     */
    final MazeConfiguration mazeConfig ;

    /**
     * This panel is used to draw all of the necessary graphics.
     */
    private MazePanelView panel;

    /**
     * Constructor
     * @param width of display
     * @param height of display
     * @param mapUnit
     * @param stepSize
     * @param seenCells
     * @param mapScale
     * @param maze
     */
    public MapDrawer(int width, int height, int mapUnit, int stepSize, Cells seenCells, int mapScale, MazeConfiguration maze){
        //System.out.println("MapDrawer: constructor called") ;
        viewWidth = width ;
        viewHeight = height ;
        this.mapUnit = mapUnit ;
        this.stepSize = stepSize ;
        this.seenCells = seenCells ;
        this.mapScale = mapScale >= 1 ? mapScale: 1 ; // 1 <= map_scale
        mazeConfig = maze ;
        // correctness considerations
        assert mazeConfig != null : "MapDrawer: maze configuration can't be null at instantiation!" ;
        assert seenCells != null : "MapDrawer: seencells can't be null at instantiation!" ;
    }

    /**
     * Constructor with default settings
     * from Constants.java for width, height, mapUnit and stepSize.
     * @param seenCells
     * @param mapScale
     * @param maze
     */
    public MapDrawer(Cells seenCells, int mapScale, MazeConfiguration maze){
        this(Constants.VIEW_WIDTH,Constants.VIEW_HEIGHT,Constants.MAP_UNIT,
                Constants.STEP_SIZE, seenCells, mapScale, maze);
    }

    public void incrementMapScale() {
        mapScale += 1 ;
    }

    public void decrementMapScale() {
        mapScale -= 1 ;
        if (1 > mapScale)
            mapScale = 1 ;
    }

    //////////////////////////////// private, internal methods //////////////////////////////
    public int getViewDX(int angle) {
        return (int) (Math.cos(radify(angle))*(1<<16));
    }
    public int getViewDY(int angle) {
        return (int) (Math.sin(radify(angle))*(1<<16));
    }
    final double radify(int x) {
        return x*Math.PI/180;
    }
    /**
     * Obtains the maximum for a given offset
     * @param offset either in x or y direction
     * @param viewLength is either viewWidth or viewHeight
     * @param mazeLength is either mazeWidth or mazeHeight
     * @return maximum that is bounded by mazeLength
     */
    public int getMaximum(int offset, int viewLength, int mazeLength) {
        int result = (viewLength-offset)/mapScale+1;
        if (result >= mazeLength)
            result = mazeLength;
        return result;
    }

    /**
     * Obtains the minimum for a given offset
     * @param offset either in x or y direction
     * @return minimum that is greater or equal 0
     */
    public int getMinimum(final int offset) {
        final int result = -offset/mapScale;
        return (result < 0) ? 0 : result;
    }

    /**
     * Calculates the offset in either x or y direction
     * @param coordinate is either x or y coordinate of current position
     * @param walkStep
     * @param viewDirection is either viewDX or viewDY
     * @param viewLength is either viewWidth or viewHeight
     * @return the offset
     */
    public int getOffset(int coordinate, int walkStep, int viewDirection, int viewLength) {
        final int tmp = coordinate*mapUnit + mapUnit/2 + mapToOffset((stepSize*walkStep),viewDirection);
        return -tmp*mapScale/mapUnit + viewLength/2;
    }

    /**
     * Maps the y index for some cell (x,y) to a y coordinate
     * for drawing.
     * @param cellY, {@code 0 <= cellY < height}
     * @param offsetY
     * @return y coordinate for drawing
     */
    public int mapToCoordinateY(int cellY, int offsetY) {
        // TODO: bug suspect: inversion with height is suspect for upside down effect on directions
        // note: (cellY*map_scale + offsetY) same as for mapToCoordinateX
        return viewHeight-1-(cellY*mapScale + offsetY);
    }

    /**
     * Maps the x index for some cell (x,y) to an x coordinate
     * for drawing.
     * @param cellX is the index of some cell, {@code 0 <= cellX < width}
     * @param offsetX
     * @return x coordinate for drawing
     */
    public int mapToCoordinateX(int cellX, int offsetX) {
        return cellX*mapScale + offsetX;
    }

    /**
     * Maps a given length and direction into an offset for drawing coordinates.
     * @param length
     * @param direction
     * @return offset
     */
    public int mapToOffset(final int length, final int direction) {
        // Signed bit shift to the right performs a division by 2^16
        // preserves the sign
        // discards the remainder as the result is int
        return (length * direction) >> 16;
    }
    /**
     * Unscale value
     * @param x
     * @return
     */
    final int unscaleViewD(int x) {
        // >> is the signed right shift operator
        // shifts input x in its binary representation
        // 16 times to the right
        // same as divide by 2^16 and discard remainder
        // preserves sign
        // essentially used here for the following mapping
        // (based on debug output observations)
        // dbg("right shift: " + x + " gives " + (x >> 16));
        // -2097152 gives -32
        // -4194304 gives -64
        // -6291456 gives -96
        // -8388608 gives -128
        // 2097152 gives 32
        // 4194304 gives 64
        // 6291456 gives 96
        // 8388608 gives 128
        return x >> 16;
    }
    /**
     * Debug output
     * @param str
     *
     * idk if i really need to update this for the refactoring project, but here we go anyways
     */
    public void dbg(String str) {
        // TODO: change this to a logger
        System.out.println("MapDrawer:"+ str);
    }

    public MazePanelView getPanel() {
        return this.panel;
    }

    public void setMazePanel(MazePanelView panel) {
        this.panel = panel;
    }

    public int getViewWidth() {
        return viewWidth;
    }

    public int getViewHeight() {
        return viewHeight;
    }

    public int getMapUnit() {
        return mapUnit;
    }

    public int getMapScale() {
        return mapScale;
    }

    public MazeConfiguration getMazeConfig() {
        return mazeConfig;
    }

    public Cells getSeenCells() {
        return seenCells;
    }
}
