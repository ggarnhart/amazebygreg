package com.example.greggarnhart.amazebygreggarnhart.gui;


import android.graphics.Color;

import generation.Seg;

public class MazeColor {

    MazeColorOptions chosenOption;
    Color color;
    int colorNum;
    int segRGBDef;
    private Seg seg;

    // used to assign colors without really using them you know.
    public enum MazeColorOptions{ RED, BLUE, GREEN, YELLOW, PINK, ORANGE, BLACK, DARKGREY, WHITE, GRAY};

    public MazeColor(){
        // nothing to see here
    }

    /**
     * Declare a maze color with a chosen color
     * @param chosenColor
     */
    public MazeColor(MazeColorOptions chosenColor){
        this.chosenOption = chosenColor;
    }

    /**
     * Set a color to a MazeColor class later on
     * @param chosenColor
     */
    public void setColor(MazeColorOptions chosenColor){
        this.chosenOption = chosenColor;
        this.setColorFromEnum(chosenColor);
    }

    /**
     * Used for MazeFileReader
     * @param col comes from MazeFileReader and is ready to be changed into an integer.
     * pretty easy
     */
    public void setColor(int col){
//        this.color = new Color(col);
    }

    // from Seg.java

    /**
     * @return an awt.Color object
     */
    public Color getColor(){
        return this.color;
    }

    /**
     * Replaced a method from Seg - used in a static context
     * @param c
     * @return
     */
    public static int getColorRGB(Color c){
        return 5;
//        return c.getRGB();
    }

    /**
     * Determine and set the color for this segment.
     * Basically copied and pasted from seg
     *
     * @param distance
     *            to exit
     * @param cc
     *            obscure
     */
    public void initColor(final int distance, final int cc) {

//        segRGBDef = seg.getRGBDef();
//
//        final int d = distance / 4;
//        // mod used to limit the number of colors to 6
//        final int rgbValue = seg.calculateRGBValue(d); // just push it over to seg's calculation.
//        switch (((d >> 3) ^ cc) % 6) {
//            case 0:
//                setColor(new Color(rgbValue, segRGBDef, segRGBDef));
//                break;
//            case 1:
//                setColor(new Color(segRGBDef, rgbValue, segRGBDef));
//                break;
//            case 2:
//                setColor(new Color(segRGBDef, segRGBDef, rgbValue));
//                break;
//            case 3:
//                setColor(new Color(rgbValue, rgbValue, segRGBDef));
//                break;
//            case 4:
//                setColor(new Color(segRGBDef, rgbValue, rgbValue));
//                break;
//            case 5:
//                setColor(new Color(rgbValue, segRGBDef, rgbValue));
//                break;
//            default:
//                setColor(new Color(segRGBDef, segRGBDef, segRGBDef));
//                break;
//        }
    }

    /**
     * @param color is sent in from another file (seg, actually!) and then sets this color to that.
     */
    public void setColor(final Color color) {
        this.color = color;
    }

    public void setSeg(Seg s){
        this.seg = s;
    }
    //// internal methods for translating our enum to an actual color ////
    private void setColorFromEnum(MazeColorOptions color){
        switch(color){
            case RED:
                this.colorNum = Color.RED;
                break;

            case BLUE:
                this.colorNum = Color.BLUE;
                break;

            case GREEN:
                this.colorNum = Color.GREEN;
                break;

            case YELLOW:
                this.colorNum = Color.YELLOW;
                break;

            case PINK:
                this.colorNum = Color.CYAN;
                break;

            case ORANGE:
                this.colorNum = Color.MAGENTA;
                break;

            case BLACK:
                this.colorNum = Color.BLACK;
                break;

            case DARKGREY:
                this.colorNum = Color.DKGRAY;
                break;

            case WHITE:
                this.colorNum = Color.WHITE;
                break;

            case GRAY:
                this.colorNum = Color.GRAY;
                break;
        }
    }
}
