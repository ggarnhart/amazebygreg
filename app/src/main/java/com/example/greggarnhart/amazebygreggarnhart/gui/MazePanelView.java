package com.example.greggarnhart.amazebygreggarnhart.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import generation.CardinalDirection;
import generation.Cells;
import generation.GlobalClass;
import generation.MazeConfiguration;

public class MazePanelView extends View {

    // Note-taking variables
    private Paint paint; // eh idrk if i need this on
    public Canvas canvas; // GLOBALIZE
    public Bitmap bitmap; // GLOBALIZE
    private boolean levelComplete;
    private MapDrawer mapDrawer;
    private FirstPersonDrawer firstPersonDrawer;
    private boolean hasScaledMap = false;

    /**
     * The first of four necessary constructors per the View superclass.
     * NOT USED
     *
     * @param context is the context that calls this view. In our case, Play Manually or Play Animation
     */
    public MazePanelView(Context context) {
        super(context);
        // not used

    }


    /**
     * This is the one that is called to create the view psa
     * @param context is the passed in context.
     * @param attrs is the passed in set of attributes.
     */
    public MazePanelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        int x = this.getWidth();
        this.bitmap = Bitmap.createBitmap(1500, 1500, Bitmap.Config.ARGB_8888); // maybe don't use these width and height stuff
        canvas = new Canvas(bitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        canvas.drawBitmap(bitmap, 0, 0, new Paint());

        invalidate();
    }

    /**
     * Used to test the color changes & shape methods of this class
     * draws the following:
     * - 1 red ball
     * - 1 green ball
     * - 1 yellow rectangle
     * - 1 blue polygon
     * - a few lines
     */
    private void littleMethod(Canvas canvas) {

        Rect r = new Rect(0, 150, canvas.getWidth(), 350);
        Paint yell = new Paint();
        yell.setColor(Color.YELLOW);

        RectF redBall = new RectF(200, 200, 600, 600);
        Paint red = new Paint();
        red.setColor(Color.RED);

        RectF greenBall = new RectF(300, 0, 400, 200);
        Paint green = new Paint();
        green.setColor(Color.GREEN);

        // triangles are harder. booooo
        // we need a starting point first!
        int x = 1000;
        int y = 1000;
        int width = 500;
        Path triangle = new Path();
        triangle.moveTo(x, y - width / 2);
        triangle.lineTo(x - width / 2, y + width / 2);
        triangle.lineTo(x + width / 2, y + width / 2);
        triangle.lineTo(x, y - width / 2);
        triangle.close();

        Paint blue = new Paint();
        blue.setColor(Color.BLUE);

        canvas.drawRect(r, yell);
        canvas.drawOval(redBall, red);
        canvas.drawOval(greenBall, green);
        canvas.drawPath(triangle, blue);
    }


    /**
     * draws over anything that is currently on the panel, makes the bottom half black and the top half gray
     */
    private void drawBackground() {
        //instantiate bottom and top
        Rect top = new Rect(0,0,1440,720);
        Paint pTop = new Paint();
        int blackReplacement = Color.parseColor("#00303D");
        pTop.setColor(blackReplacement);

        Rect bottom = new Rect(0, 720, 1440, 1440);
        Paint pBottom = new Paint();
        pBottom.setColor(Color.GRAY);


        canvas.drawRect(bottom, pBottom); // bottom

        //top
        canvas.drawRect(top, pTop);
    }

    /**
     * Draws the FIRST PERSON VIEW on the screen during the game
     *
     * @param panel     for drawing on the buffer image
     * @param x         coordinate of current position, only used to set viewX
     * @param y         coordinate of current position, only used to set viewY
     * @param ang       gives the current viewing angle
     * @param walkStep, only used to set viewX and viewY
     */
    public void draw(MazePanelView panel, int x, int y, int walkStep, int ang) {

        Canvas c = panel.getCanvas();
        if (null == c) {
            System.out.println("MazePanelView.draw: can't get graphics object to draw on, skipping draw op");
            return;
        } else {
            this.canvas = c;
            // update fields angle, viewx, viewy for current position and viewing angle
            firstPersonDrawer.setAngle(ang);
            firstPersonDrawer.setView(x, y, walkStep);

            // update graphics: draw background figure (black on bottom, gray on top)
            this.drawBackground();
            // set color to white and draw what ever can be seen from the current position
            if(null == paint){
                paint = new Paint();
            }
            paint.setColor(Color.WHITE); // i believe this happens elsewhere.

            // reset the set of ranges to a single new element (0,width-1)
            // to cover the full width of the view
            // as we have not drawn any polygons (walls) yet.
           firstPersonDrawer.rSet.set(0, firstPersonDrawer.getViewWidth()-1);

            // debug: reset counters
            firstPersonDrawer.resetCounters();
            firstPersonDrawer.drawAllVisibleSectors(firstPersonDrawer.getRoot());
        }
    }

    /**
     * IMITATION OF GETBUFFERGRAPHICS
     *
     * Obtains a graphics (which in android is the canvas)
     * object that can be used for drawing.
     * This MazePanel object internally stores the graphics (canvas) object
     * and will return the same graphics object over multiple method calls.
     * The graphics (canvas) object acts like a notepad (ah the bitmap) (aha!) where all clients draw
     * on to store their contribution to the overall image that is to be
     * delivered later.
     * To make the drawing visible on screen, one needs to trigger
     * a call of the ondraw method, which happens
     * when calling the update method. (with the use of invalidate())
     * @return graphics (bitmap) object to draw on, null if impossible to obtain image
     */
    public Canvas getCanvas(){
        if(null == this.canvas){
            if(null == this.bitmap ){
//                bitmap = new Bitmap.createBitmap(1000, 1000, Bitmap.Config.ARGB_8888);
                return null;
            }
        }
        if(null == this.canvas){

        }
        else{

        }

        return this.canvas;  // i guess :)
    }

    /* Map Drawer Stuff */

    /**
     * Draws the CURRENT MAP on top of the first person view.
     * Method assumes that we are in the playing state and that
     * the map mode is switched on.
     *
     * @param panel
     * @param x
     * @param y
     * @param angle
     * @param walkStep     is a counter between 0, 1, 2, ..., 3
     *                     for in between stages for a walk operation, needed to obtain
     *                     exact location in map
     * @param showMaze     if true, highlights already seen walls in white
     * @param showSolution if true shows a path to the exit as a yellow line,
     *                     otherwise path is not shown.
     */
    public void draw(MazePanelView panel, int x, int y, int angle, int walkStep,
                     boolean showMaze, boolean showSolution) {
        Canvas c = panel.getCanvas();

        if (null == c) {
            System.out.println("MazePanelView.draw: can't get graphics object to draw on, skipping draw op");
            return;
        } else {
            final int viewDX = mapDrawer.getViewDX(angle);
            final int viewDY = mapDrawer.getViewDY(angle);
            drawMap(panel.canvas, x, y, walkStep, viewDX, viewDY, showMaze, showSolution);
            drawCurrentLocation(panel.canvas, viewDX, viewDY);
        }

    }

    /**
     * Sorts out the 2D-Graphics debacle. Nice!
     *
     * @param path
     * @param paint
     */
    public void drawPath(Path path, Paint paint) {
        canvas.drawPath(path, paint);
    }


    public void setFirstPersonDrawer(FirstPersonDrawer firstPersonView) {
        this.firstPersonDrawer = firstPersonView;
    }

    public void setMapDrawer(MapDrawer mapView) {
        this.mapDrawer = mapView;
    }

    /**
     * Adjusts the internal map scale setting for the map view.
     *
     * @param increment if true increase, otherwise decrease scale for map
     */
    private void adjustMapScale(boolean increment) {
        if (increment) {
            mapDrawer.incrementMapScale();
        } else {
            mapDrawer.decrementMapScale();
        }
    }


    /**
     * Helper method for draw, called if map_mode is true, i.e. the users wants to see the overall map.
     * The map is drawn only on a small rectangle inside the maze area such that only a part of the map is actually shown.
     * Of course a part covering the current location needs to be displayed.
     * The current cell is (px,py). There is a viewing direction (view_dx, view_dy).
     * @param canvas graphics handler to manipulate screen
     * @param px current position, x index
     * @param py current position, y index
     */
    private void drawMap(Canvas canvas, int px, int py, int walkStep,
                         int viewDX, int viewDY, boolean showMaze, boolean showSolution) {

        if(!hasScaledMap){
            hasScaledMap = true;
        }

        // dimensions of the maze in terms of cell ids
        final int mazeWidth = mapDrawer.getMazeConfig().getWidth() ;
        final int mazeHeight = mapDrawer.getMazeConfig().getHeight() ;

        // in the awt version, you'd set the canvas color, but here, we need a paint.
        // fortunately, this is rather simple.
        Paint p = new Paint();
        p.setColor(Color.WHITE);


        // note: 1/2 of width and height is the center of the screen
        // the whole map is centered at the current position
        final int offsetX = mapDrawer.getOffset(px, walkStep, viewDX, mapDrawer.getViewWidth());
        final int offsetY = mapDrawer.getOffset(py, walkStep, viewDY, mapDrawer.getViewHeight());

        // We need to calculate bounds for cell indices to consider
        // for drawing. Since not the whole maze may be visible
        // for the given screen size and the current position (px,py)
        // is fixed to the center of the drawing area, we need
        // to find the min and max indices for cells to consider.
        // compute minimum for x,y
        final int minX = mapDrawer.getMinimum(offsetX);
        final int minY = mapDrawer.getMinimum(offsetY);
        // compute maximum for x,y
        final int maxX = mapDrawer.getMaximum(offsetX, mapDrawer.getViewWidth(), mazeWidth);
        final int maxY = mapDrawer.getMaximum(offsetY, mapDrawer.getViewHeight(), mazeHeight);

        // iterate over integer grid between min and max of x,y indices for cells
        for (int y = minY; y <= maxY; y++)
            for (int x = minX; x <= maxX; x++) {
                // starting point of line
                float startX = mapDrawer.mapToCoordinateX(x, offsetX);
                float startY = mapDrawer.mapToCoordinateY(y, offsetY);

                // draw horizontal line
                boolean theCondition = (x < mazeWidth) && ((y < mazeHeight) ?
                        mapDrawer.getMazeConfig().hasWall(x, y, CardinalDirection.North) :
                        mapDrawer.getMazeConfig().hasWall(x, y - 1, CardinalDirection.South));

                p.setColor(mapDrawer.getSeenCells().hasWall(x,y, CardinalDirection.North) ? Color.WHITE : Color.WHITE);

                if ((mapDrawer.getSeenCells().hasWall(x,y, CardinalDirection.North) || showMaze) && theCondition)
                    canvas.drawLine(startX, startY, startX + mapDrawer.getMapScale(), startY, p);

                // draw vertical line
                theCondition = (y < mazeHeight) && ((x < mazeWidth) ?
                        mapDrawer.getMazeConfig().hasWall(x, y, CardinalDirection.West) :
                        mapDrawer.getMazeConfig().hasWall((x - 1), y, CardinalDirection.East));

                p.setColor(mapDrawer.getSeenCells().hasWall(x,y, CardinalDirection.West) ? Color.WHITE : Color.WHITE);
                p.setStyle(Paint.Style.STROKE);

                if ((mapDrawer.getSeenCells().hasWall(x,y, CardinalDirection.West) || showMaze) && theCondition)
                    canvas.drawLine(startX, startY, startX, startY - mapDrawer.getMapScale(), p);
            }

        if (showSolution) {
            drawSolution(canvas, offsetX, offsetY, px, py) ;
        }
    }

    /**
     * Draws a yellow line to show the solution on the overall map.
     * Method is only called if in state playing and map_mode
     * and showSolution are true.
     * Since the current position is fixed at the center of the screen,
     * all lines on the map are drawn with some offset.
     * @param canvas to draw lines on
     * @param offsetX is the offset for x coordinates
     * @param offsetY is the offset for y coordinates
     * @param px is the current position, an index x for a cell
     * @param py is the current position, an index y for a cell
     */
    private void drawSolution(Canvas canvas, int offsetX, int offsetY, int px, int py) {

        if (!mapDrawer.getMazeConfig().isValidPosition(px, py)) {
            mapDrawer.dbg(" Parameter error: position out of bounds: (" + px + "," +
                    py + ") for maze of size " + mapDrawer.getMazeConfig().getWidth() + "," +
                    mapDrawer.getMazeConfig().getHeight()) ;
            return ;
        }
        // current position on the solution path (sx,sy)
        int sx = px;
        int sy = py;
        int distance = mapDrawer.getMazeConfig().getDistanceToExit(sx, sy);

        Paint yellow = new Paint();

        yellow.setColor(Color.YELLOW);

        // while we are more than 1 step away from the final position
        while (distance > 1) {
            // find neighbor closer to exit (with no wall in between)
            int[] neighbor = mapDrawer.getMazeConfig().getNeighborCloserToExit(sx, sy) ;
            if (null == neighbor)
                return ; // error
            // scale coordinates, original calculation:
            // x-coordinates
            // nx1     == sx*map_scale + offx + map_scale/2;
            // nx1+ndx == sx*map_scale + offx + map_scale/2 + dx*map_scale == (sx+dx)*map_scale + offx + map_scale/2;
            // y-coordinates
            // ny1     == view_height-1-(sy*map_scale + offy) - map_scale/2;
            // ny1+ndy == view_height-1-(sy*map_scale + offy) - map_scale/2 + -dy * map_scale == view_height-1 -((sy+dy)*map_scale + offy) - map_scale/2
            // current position coordinates
            //int nx1 = sx*map_scale + offx + map_scale/2;
            //int ny1 = view_height-1-(sy*map_scale + offy) - map_scale/2;
            //
            // we need to translate the cell indices x and y into
            // coordinates for drawing, the yellow lines is centered
            // so 1/2 of the size of the cell needs to be added to the
            // top left corner of a cell which is + or - map_scale/2.
            float nx1 = mapDrawer.mapToCoordinateX(sx,offsetX) + mapDrawer.getMapScale()/2;
            float ny1 = mapDrawer.mapToCoordinateY(sy,offsetY) - mapDrawer.getMapScale()/2;
            // neighbor position coordinates
            //int nx2 = neighbor[0]*map_scale + offx + map_scale/2;
            //int ny2 = view_height-1-(neighbor[1]*map_scale + offy) - map_scale/2;
            float nx2 = mapDrawer.mapToCoordinateX(neighbor[0],offsetX) + mapDrawer.getMapScale()/2;
            float ny2 = mapDrawer.mapToCoordinateY(neighbor[1],offsetY) - mapDrawer.getMapScale()/2;
            canvas.drawLine(nx1, ny1, nx2, ny2, yellow);

            // update loop variables for current position (sx,sy)
            // and distance d for next iteration
            sx = neighbor[0];
            sy = neighbor[1];
            distance = mapDrawer.getMazeConfig().getDistanceToExit(sx, sy) ;
        }
    }

    /**
     * Draws a red circle at the center of the screen and
     * an arrow for the current direction.
     * It always reside on the center of the screen.
     * The map drawing moves if the user changes location.
     * The size of the overall visualization is limited by
     * the size of a single cell to avoid that the circle
     * or arrow visually collide with an adjacent wall on the
     * map visualization.
     * @param canvas to draw on
     */
    private void drawCurrentLocation(Canvas canvas, int viewDX, int viewDY) {
        Paint red = new Paint();
        red.setColor(Color.RED);

        // draw oval of appropriate size at the center of the screen
        int centerX = mapDrawer.getViewWidth()/2; // center x
        int centerY = mapDrawer.getViewHeight()/2; // center y
        int diameter = mapDrawer.getMapScale()/2; // circle size
        // we need the top left corner of a bounding box the circle is in
        // and its width and height to draw the circle
        // top left corner is (centerX-radius, centerY-radius)
        // width and height is simply the diameter
        // cannot do an oval like this. We need a rect
//        canvas.fillOval(centerX-diameter/2, centerY-diameter/2, diameter, diameter);  NOTE: THIS LINE NOT USED AS THE NEXT FEW LINES TAKE CARE OF THIS (only for learning purposes)

        // so, let's do this.
        RectF oval = new RectF(centerX-diameter/2, centerY-diameter/2, centerX+diameter/2, centerY+diameter/2);
        canvas.drawOval(oval, red);


        // draw a red arrow with the oval to show current direction
        drawArrow(canvas, viewDX, viewDY, centerX, centerY);
    }

    /**
     * Draws an arrow either in horizontal or vertical direction.
     * @param canvas to draw on
     * @param viewDX is the current viewing direction, x coordinate
     * @param viewDY is the current viewing direction, y coordinate
     * @param startX is the x coordinate of the starting point
     * @param startY is the y coordinate of the starting point
     */
    private void drawArrow(Canvas canvas, int viewDX, int viewDY,
                           final float startX, final float startY) {
        // calculate length and coordinates for main line
        final int arrowLength = mapDrawer.getMapScale()*7/16; // arrow length, about 1/2 map_scale
        final float tipX = startX + mapDrawer.mapToOffset(arrowLength, viewDX);
        final float tipY = startY - mapDrawer.mapToOffset(arrowLength, viewDY);
        // draw main line, goes from starting (x,y) to end (tipX,tipY)
        Paint p = new Paint();
        p.setColor(Color.RED);
        canvas.drawLine(startX, startY, tipX, tipY, p);
        // calculate length and positions for 2 lines pointing towards (tipX,tipY)
        // find intermediate point (tmpX,tmpY) on main line
        final int length = mapDrawer.getMapScale()/4;
        final float tmpX = startX + mapDrawer.mapToOffset(length, viewDX);
        final float tmpY = startY - mapDrawer.mapToOffset(length, viewDY);
        // find offsets at intermediate point for 2 points orthogonal to main line
        // negative sign used for opposite direction
        // note the flip between x and y for view_dx and view_dy
		/*
		final int offsetX = -(length * view_dy) >> 16;
		final int offsetY = -(length * view_dx) >> 16;
		*/
        final int offsetX = mapDrawer.mapToOffset(length, -viewDY);
        final int offsetY = mapDrawer.mapToOffset(length, -viewDX);
        // draw two lines, starting at tip of arrow
        canvas.drawLine(tipX, tipY, tmpX + offsetX, tmpY + offsetY, p);
        canvas.drawLine(tipX, tipY, tmpX - offsetX, tmpY - offsetY, p);
    }
}
