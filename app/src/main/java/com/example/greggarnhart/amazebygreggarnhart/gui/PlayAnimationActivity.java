package com.example.greggarnhart.amazebygreggarnhart.gui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.greggarnhart.amazebygreggarnhart.R;

import generation.GlobalClass;
import generation.MazeConfiguration;

public class PlayAnimationActivity extends AppCompatActivity {

    public static final String STEPS_TAKEN = "com.example.greggarnhart.amazebygreggarnhart.Steps";
    public static final String SHORTEST_PATH = "com.example.greggarnhart.amazebygreggarnhart.ShortestPath";
    public static final String CELLS_CONSUMED = "com.example.greggarnhart.amazebygreggarnhart.Cells";
    public static final String ROBOT_USED = "com.example.greggarnhart.amazebygreggarnhart.RobotUsed";

    private boolean mapShowing = false;
    private boolean solutionShowing = false;
    private int energy = 3000;
    private int moves = 0;
    private int shortestPath = 12;
    private boolean robotUsed = true;

    private BasicRobot robbie;
    private RobotDriver driver;

    private MazePanelView panel;
    private MazeConfiguration mazeConfiguration;
    private StateAnimation stateAnimation;

    Handler handler;
    BackgroundAnimator backgroundAnimator;

    private boolean allDone = false;
    private boolean failed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_animation);

        GlobalClass gc = (GlobalClass) getApplicationContext();
        mazeConfiguration = gc.mazeConfiguration;
        if(gc.nightMode){
            nightMode();
        }

        TextView robotLabel = findViewById(R.id.robotLabel);
        Intent pastIntent = getIntent();
        String driverType = pastIntent.getStringExtra(GeneratingActivity.DRIVER);
        robotLabel.setText("You are being driven by a " + driverType);

        stateAnimation = new StateAnimation();
        stateAnimation.setMazeConfiguration(this.mazeConfiguration);

        this.robbie = new BasicRobot(stateAnimation, mazeConfiguration);

        assignDriverType(driver, driverType);
        robbie.setSensors();

        panel = findViewById(R.id.mazePanelView);

        stateAnimation.startAnimation(this, panel);

        backgroundAnimator = new BackgroundAnimator(driver, robbie);

         handler = new Handler() {
             @Override
             public void handleMessage(Message message) {
//                 if(0 <= message.arg2){
//                     // update the text and stuff
//                     updateEnergyAndMoves((int)robbie.getBatteryLevel(), robbie.getOdometerReading());
//                 }

                Log.v("onCreate", "WE MADE IT");
                // move to the finish
                 // update the variables too probably
                 if(allDone){
                     handler.removeCallbacks(backgroundAnimator);
                     win();
                 }

                 else if(failed){
                     handler.removeCallbacks(backgroundAnimator);
                     Log.v("UI", "Moving to LOSE haha");
                     lose();
                 }

             }
         };


    }


    class BackgroundAnimator implements Runnable{
        private RobotDriver driver;
        private Robot robbie;

        BackgroundAnimator(RobotDriver driver, Robot robot){
            this.driver = driver;
            this.robbie = robot;
        }
        @Override
        public void run() {
            try {
                this.driver.drive2Exit();
                Message message = Message.obtain();
                message.arg1 = this.robbie.getOdometerReading();
                message.arg2 = (int) this.robbie.getBatteryLevel();
                handler.sendMessage(message);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(this.robbie.isAtExit()){
                boolean test = this.robbie.isAtExit();
                Log.v("At Exit", "HEY we made it to the exit!");
                allDone = true;
                this.robbie.rotateToViewExit();
                robbie.move(1, false);
                Message message = Message.obtain();
                message.arg1 = 1;
                handler.sendMessage(message);

            }
            else if(this.robbie.getBatteryLevel() < 5){
                Log.v("No Energy", "Dang robbie doesn't have enough energy to move again");
                failed = true;
                Message message = Message.obtain();
                message.arg1 = 0;
                handler.sendMessage(message);
            }

            handler.postDelayed(this, 800);
        }
    }

    private void assignDriverType(RobotDriver driver, String driverType) {
        switch (driverType) {
            case "Wizard":
                Log.v("AssignDriverType", "Hey nice we are doing the animation with a wizard");
                this.driver = new Wizard(this.robbie, this.mazeConfiguration);


                break;
            case "Explorer":
                Log.v("AssignDriverType", "WOW time to explorer with the ~Explorer~");
                this.driver = new Explorer(this.robbie);
                break;
            case "WallFollower":
                Log.v("AssignDriverType", "Gosh i wanna bang my head into a wall lol");
                this.driver = new WallFollower(this.robbie);
                break;
            default:
                this.driver = new Wizard(this.robbie, this.mazeConfiguration);
                break;
        }

    }

    public void startDriver(View view) throws Exception {
        Button startButton = findViewById(R.id.startDriverButton);
        startButton.setText("driving");
        startButton.setEnabled(false);
        this.robbie.setInPlayingState(true);
        stateAnimation.inPlayState = true;

        Button pauseButton = findViewById(R.id.pauseButton);
        pauseButton.setText("Pause");
        pauseButton.setEnabled(true);

        backgroundAnimator.run();


    }

    public void pauseDriver(View view){
        handler.removeCallbacks(backgroundAnimator);
        Button startButton = findViewById(R.id.startDriverButton);
        startButton.setText("Resume Driving");
        startButton.setEnabled(true);

        Button pauseButton = findViewById(R.id.pauseButton);
        pauseButton.setText("paused");
        pauseButton.setEnabled(false);

    }

    public void toggleMap(View view) {
        Button mapButton = findViewById(R.id.mapToggle);

        if (mapShowing) {
            // hide map


            mapShowing = false;
            mapButton.setText("Show Map");

        } else {
            // show map

            mapShowing = true;
            mapButton.setText("Hide Map");
        }
        Log.v("Toggle Map Button", "Button Tapped!");
    }

    public void toggleSolution(View view) {

        Button mapButton =  findViewById(R.id.mapToggle);
        Button solutionButton =  findViewById(R.id.solutionToggle);

        if (!solutionShowing) {
            solutionShowing = true;
            stateAnimation.toggleSolution();
            solutionButton.setText("Hide Solution");

        } else {
            solutionShowing = false;
            stateAnimation.toggleSolution();
            solutionButton.setText("Show Solution");
        }

        if (!mapShowing) {
            stateAnimation.toggleMap();
            mapShowing = true;
            mapButton.setText("Hide Map");
        }

        Log.v("Toggle Solution Button", "Button Tapped!");
    }

    public void win(View view) {
        Intent intent = new Intent(this, WinningActivity.class);

        String stepsTaken = moves + "";
        int cellsUsed = 3000 - energy;
        String cellsUsedString = "" + cellsUsed + "";
        String shortestPathString = shortestPath + "";

        intent.putExtra(STEPS_TAKEN, stepsTaken);
        intent.putExtra(SHORTEST_PATH, shortestPathString);
        intent.putExtra(CELLS_CONSUMED, cellsUsedString);
        intent.putExtra(ROBOT_USED, robotUsed);

        startActivity(intent);

        Log.v("Win Button", "Button Tapped, moving to Win Activity!");
    }

    public void lose(View view) {
        Intent intent = new Intent(this, LosingActivity.class);

        String stepsTaken = moves + "";
        int cellsUsed = 3000 - energy;
        String cellsUsedString = "" + cellsUsed + "";
        String shortestPathString = shortestPath + "";

        intent.putExtra(STEPS_TAKEN, stepsTaken);
        intent.putExtra(SHORTEST_PATH, shortestPathString);
        intent.putExtra(CELLS_CONSUMED, cellsUsedString);
        intent.putExtra(ROBOT_USED, robotUsed);

        startActivity(intent);

        Log.v("Lose Button", "Button Tapped, moving to Lose Activity!");

    }

    private void updateEnergyAndMoves(int energy, int moves) {
        this.moves = moves;

        TextView movesLabel = (TextView) findViewById(R.id.movesTaken);
        if (moves == 1) {
            movesLabel.setText("1 move taken");
        } else {
            movesLabel.setText(moves + " moves taken");
        }
    }

    /**
     * If a robot runs out of energy and isn't outside, then it is game over.
     *
     * @param xCoord   is the robot's x coordinate
     * @param yCoord   is the robot's y coordinate
     * @param actuator is the sort of 'override' boolean. If a method sends a true actuator, the game is over.
     *                 this could happen when a batter 1 energy cell left, but wants to rotate and can't.
     */
    public void checkGameOver(int xCoord, int yCoord, boolean actuator) {
//        if((robot.getBatteryLevel() <= 0 && !((StatePlaying)states[2]).isOutside(xCoord, yCoord)) || actuator) {
//            this.robot.setBatteryLevel(3000); // maybe make this reference something else idk it doesn't seem like it'll change
//            this.robot.resetOdometer();
//            this.switchFromPlayingToLosing(this.robot.getOdometerReading());
//        }
    }

    public void win() {
        Intent intent = new Intent(this, WinningActivity.class);

        String stepsTaken = ""+ this.robbie.getOdometerReading();
        int cellsUsed = 3000 -(int) this.robbie.getBatteryLevel();
        String cellsUsedString = "" + cellsUsed + "";
        String shortestPathString = shortestPath + "";

        intent.putExtra(STEPS_TAKEN, stepsTaken);
        intent.putExtra(SHORTEST_PATH, shortestPathString);
        intent.putExtra(CELLS_CONSUMED, cellsUsedString);
        intent.putExtra(ROBOT_USED, robotUsed);

        startActivity(intent);

        Log.v("Win Button", "Button Tapped, moving to Win Activity!");
    }

    public void lose() {
        Intent intent = new Intent(this, LosingActivity.class);

        String stepsTaken = this.robbie.getOdometerReading() + "";
        int cellsUsed = 3000 - (int) this.robbie.getBatteryLevel();
        String cellsUsedString = "" + cellsUsed + "";
        String shortestPathString = shortestPath + "";

        intent.putExtra(STEPS_TAKEN, stepsTaken);
        intent.putExtra(SHORTEST_PATH, shortestPathString);
        intent.putExtra(CELLS_CONSUMED, cellsUsedString);
        intent.putExtra(ROBOT_USED, robotUsed);

        startActivity(intent);

        Log.v("Lose Button", "Button Tapped, moving to Lose Activity!");

    }

    private void nightMode() {
        int deepBlue = Color.parseColor("#00303D");
        getWindow().getDecorView().setBackgroundColor(deepBlue);

        String fontColor = "#F3F3F3";
        String buttonColor = "#37CFCA";

        int fontColorInt = Color.parseColor(fontColor);
        int buttonColorInt = Color.parseColor(buttonColor);

        Button start = findViewById(R.id.startDriverButton);
        TextView textView = findViewById(R.id.robotLabel);

        start.setBackgroundColor(buttonColorInt);
        textView.setTextColor(fontColorInt);
    }
}
