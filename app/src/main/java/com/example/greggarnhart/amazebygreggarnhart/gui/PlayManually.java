package com.example.greggarnhart.amazebygreggarnhart.gui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.greggarnhart.amazebygreggarnhart.R;

import generation.CardinalDirection;
import generation.Cells;
import generation.GlobalClass;
import generation.MazeConfiguration;
import com.example.greggarnhart.amazebygreggarnhart.gui.Robot.Turn;

public class PlayManually extends AppCompatActivity {

    public static final String STEPS_TAKEN = "com.example.greggarnhart.amazebygreggarnhart.Steps";
    public static final String SHORTEST_PATH = "com.example.greggarnhart.amazebygreggarnhart.ShortestPath";
    public static final String CELLS_CONSUMED = "com.example.greggarnhart.amazebygreggarnhart.Cells";
    public static final String ROBOT_USED = "com.example.greggarnhart.amazebygreggarnhart.RobotUsed";

    private boolean mapShowing = false;
    private boolean solutionShowing = false;
    private int energy = 3000;
    private int moveForwardCost = 5;
    private int rotateCost = 3;
    private int moves = 0;
    private int shortestPath = 12;
    private boolean robotUsed = false;

    private MazePanelView panel;


    // copy pasted values
    private MazeConfiguration mazeConfig;

    int angle; // current viewing angle, east == 0 degrees
    int walkStep; // counter for intermediate steps within a single step forward or backward
    Cells seenCells; // a matrix with cells to memorize which cells are visible from the current point of view
    // the FirstPersonDrawer obtains this information and the MapDrawer uses it for highlighting currently visible walls on the map
    FirstPersonDrawer firstPersonDrawer;
    private MapDrawer mapDrawer;

    private StatePlaying statePlaying;


    boolean started;

    private BasicRobot robbie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_manually);


        GlobalClass gc = (GlobalClass) getApplicationContext();
        this.mazeConfig = gc.mazeConfiguration;
        if(gc.nightMode){
            nightMode();
        }

        panel = findViewById(R.id.mazePanelView);

        statePlaying = new StatePlaying();
        statePlaying.setMazeConfiguration(mazeConfig);

        statePlaying.start(this, panel);
    }

    /**
     *
     *
     * @param view
     */
    public void leftButton(View view) {
        statePlaying.rotate(1);

        updateEnergyAndMoves();

        Log.v("Left Button", "Button Tapped!");

    }

    /**
     * Updated to interact with the "walk" function
     *
     * @param view
     */
    public void upButton(View view) {
        statePlaying.walk(1);
        moves++;
        if(statePlaying.isOutside(statePlaying.px, statePlaying.py)){
            win();
        }

        updateEnergyAndMoves();

        Log.v("Up Button", "Button Tapped!");
    }

    public void rightButton(View view) {
        statePlaying.rotate(-1);
        energy = energy - rotateCost;
        moves++;
        updateEnergyAndMoves();

        Log.v("Right Button", "Button Tapped!");
    }

    public void bottomButton(View view) {
        statePlaying.walk(-1);
        if(statePlaying.isOutside(statePlaying.px, statePlaying.py)){
            win();
        }
        moves++;
        updateEnergyAndMoves();

        Log.v("Bottom Button", "Button Tapped - rotating 180 degrees!");
    }

    public void toggleMap(View view) {

        Button mapButton = findViewById(R.id.mapToggle);

        if (mapShowing) {
            // hide map
            statePlaying.toggleMap();
            mapShowing = false;
            mapButton.setText("Show Map");

        } else {
            // show map
            statePlaying.toggleMap();
            mapShowing = true;
            mapButton.setText("Hide Map");
        }

        Log.v("Toggle Map Button", "Button Tapped!");
    }

    public void toggleSolution(View view) {

        Button mapButton = findViewById(R.id.mapToggle);
        Button solutionButton = findViewById(R.id.solutionToggle);

        if (!solutionShowing) {
            solutionShowing = true;
            statePlaying.toggleSolution();
            solutionButton.setText("Hide Solution");

        } else {
            solutionShowing = false;
            statePlaying.toggleSolution();
            solutionButton.setText("Show Solution");
        }

        if (!mapShowing) {
            statePlaying.toggleMap();
            mapShowing = true;
            mapButton.setText("Hide Map");
        }

        Log.v("Toggle Solution Button", "Button Tapped!");
    }

    public void win(View view) {
        Intent intent = new Intent(this, WinningActivity.class);

        String stepsTaken = moves + "";
        int cellsUsed = 3000 - energy;
        String cellsUsedString = "" + cellsUsed + "";
        String shortestPathString = shortestPath + "";

        intent.putExtra(STEPS_TAKEN, stepsTaken);
        intent.putExtra(SHORTEST_PATH, shortestPathString);
        intent.putExtra(CELLS_CONSUMED, cellsUsedString);
        intent.putExtra(ROBOT_USED, robotUsed);

        startActivity(intent);

        Log.v("Win Button", "Button Tapped, moving to Win Activity!");
    }

    public void win() {
        Intent intent = new Intent(this, WinningActivity.class);

        String stepsTaken = moves + "";
        int cellsUsed = 3000 - energy;
        String cellsUsedString = "" + cellsUsed + "";
        String shortestPathString = shortestPath + "";

        intent.putExtra(STEPS_TAKEN, stepsTaken);
        intent.putExtra(SHORTEST_PATH, shortestPathString);
        intent.putExtra(CELLS_CONSUMED, cellsUsedString);
        intent.putExtra(ROBOT_USED, robotUsed);

        startActivity(intent);

        Log.v("Win Button", "Button Tapped, moving to Win Activity!");
    }

    public void lose(View view) {
        Intent intent = new Intent(this, LosingActivity.class);

        String stepsTaken = moves + "";
        int cellsUsed = 3000 - energy;
        String cellsUsedString = "" + cellsUsed + "";
        String shortestPathString = shortestPath + "";

        intent.putExtra(STEPS_TAKEN, stepsTaken);
        intent.putExtra(SHORTEST_PATH, shortestPathString);
        intent.putExtra(CELLS_CONSUMED, cellsUsedString);
        intent.putExtra(ROBOT_USED, robotUsed);

        startActivity(intent);
        Log.v("Lose Button", "Button Tapped, moving to Lose Activity!");
    }

    private void updateEnergyAndMoves() {
        TextView movesLabel = (TextView) findViewById(R.id.movesTaken);

        if (moves == 1) {
            movesLabel.setText("1 move taken");
        } else {
            movesLabel.setText(moves + " moves taken");
        }
    }

    /**
     * If a robot runs out of energy and isn't outside, then it is game over.
     *
     * @param xCoord   is the robot's x coordinate
     * @param yCoord   is the robot's y coordinate
     * @param actuator is the sort of 'override' boolean. If a method sends a true actuator, the game is over.
     *                 this could happen when a batter 1 energy cell left, but wants to rotate and can't.
     */
    public void checkGameOver(int xCoord, int yCoord, boolean actuator) {
//        if((robot.getBatteryLevel() <= 0 && !((StatePlaying)states[2]).isOutside(xCoord, yCoord)) || actuator) {
//            this.robot.setBatteryLevel(3000); // maybe make this reference something else idk it doesn't seem like it'll change
//            this.robot.resetOdometer();
//            this.switchFromPlayingToLosing(this.robot.getOdometerReading());
//        }
    }

    public void nightMode(){
        int deepBlue = Color.parseColor("#00303D");
        getWindow().getDecorView().setBackgroundColor(deepBlue);

        String fontColor = "#F3F3F3";
        String buttonColor = "#37CFCA";

        int fontColorInt = Color.parseColor(fontColor);
        int buttonColorInt = Color.parseColor(buttonColor);

        TextView movesLabel = findViewById(R.id.movesTaken);
        Button upButton = findViewById(R.id.upButton);
        Button rightButton = findViewById(R.id.rightButton);
        Button downButton = findViewById(R.id.downButton);
        Button leftButton = findViewById(R.id.leftButton);


        movesLabel.setTextColor(fontColorInt);
        upButton.setBackgroundColor(buttonColorInt);
        rightButton.setBackgroundColor(buttonColorInt);
        downButton.setBackgroundColor(buttonColorInt);
        leftButton.setBackgroundColor(buttonColorInt);
    }


}
