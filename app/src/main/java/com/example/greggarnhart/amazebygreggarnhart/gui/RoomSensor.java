package com.example.greggarnhart.amazebygreggarnhart.gui;

import generation.Cells;
import generation.MazeConfiguration;
/**
 * Room sensors don't really do much. They just tell the robot if they are currently in the room.
 * @author Greg Garnhart
 *
 */
public class RoomSensor implements Sensor {

    /**
     * This maze configuration is set when a new robot is paired.
     * It allows us to see if we are in a room.
     */
    MazeConfiguration mazeConfigs;

    /**
     * The robot the sensor is paired with.
     */
    Robot robbie;

    /**
     * Instantiates a RoomSensor
     */
    public RoomSensor() {

    }

    /**
     * Instantiates a RoomSensor and pairs it with a robot.
     * @param r is the robot that is instantiating the sensor.
     */
    public RoomSensor(Robot r) {
        this.robbie = r;
        this.mazeConfigs = r.getMazeConfig();
    }

    /**
     * Sets the sensor to a particular robot.
     */
    public void setRobot(Robot r) {
        this.robbie = r;
        this.mazeConfigs = r.getMazeConfig();
    }

    /**
     * Returns whether or not the robot is in a room.
     * It takes the coordinates into account and can then tell if it is in a room
     * by looking at the controller and stuff.
     *
     * yeah actually this is super easy because the cells class already has a method for this,
     * so we just get to use that.
     *
     * YAY
     */
    @Override
    public boolean sense() {
        int [] position = new int [2];
        try {
            position = this.robbie.getCurrentPosition();
            // return this.mazeConfigs.getMazecells().isInRoom(position[0], position[1]);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Cells cells = this.mazeConfigs.getMazecells();
        boolean answer = cells.isInRoom(position[0], position[1]);
        return answer;
    }

    /**
     * Not needed. May delete in refactoring stage.
     */
    @Override
    public int senseDirection() {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * Yeah this one doesn't do that.
     * This is here because of poor interface design.
     * Apologies.
     */
    @Override
    public boolean senseExit() {
        // TODO Auto-generated method stub
        return false;
    }

}
