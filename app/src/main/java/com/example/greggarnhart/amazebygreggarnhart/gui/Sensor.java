package com.example.greggarnhart.amazebygreggarnhart.gui;

/**
 *
 * @author Greg Garnhart
 * The sensor interface allows for easy implementation of directional and room sensor classes.
 *
 * Pairs with the an instantiated robot to allow for easy diagnosis of the current maze situation.
 *
 */
public interface Sensor {

    /**
     * Pairs a sensor to a robot.
     */
    void setRobot(Robot r);

    /**
     * Performs this sensors 'sense' operation.
     * In a room sensor class, this would allow for the room sensor to see if there is a room in front.
     * In a directional sensor, this would check to see if it were immediately blocked or not.
     *
     * @return whether or not the thing the sensor is sensing is there
     */
    boolean sense();

    /**
     * @return how far to obstruction
     */
    int senseDirection();

    /**
     * Used for canSeeExit();
     * The direction sensor has all the necessary details.
     */
    boolean senseExit();
}
