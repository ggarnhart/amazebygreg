package com.example.greggarnhart.amazebygreggarnhart.gui;

import android.util.Log;

import generation.Distance;
import com.example.greggarnhart.amazebygreggarnhart.gui.Constants.UserInput;
import com.example.greggarnhart.amazebygreggarnhart.gui.Robot.Turn;

/**
 * The WallFollower class is a RobotDriver that drives along the left wall until the exit is reach.
 * It's pretty simple.
 *
 * CRC:
 * Responsibilities:
 * - Locates left wall.
 * - Moves forward
 * - Turns when necessary
 * - Reaches exit.
 * YAY
 *
 * Relationships
 * - RobotDriver Interface
 * - Some sort of Robot (maybe basic robot, maybe not)
 * - WallFollowerTest (this is a generous 'collaborator' but so i remember to do it later...)
 * @author greggarnhart
 *
 */
public class WallFollower implements RobotDriver {

    /**
     * WallFollowers drive basic robots.
     */
    private Robot robbie;
    private boolean lastMoveLeft = false;
    private boolean needsToMove = false;
    private int uTurn = 0;
    private boolean firstOneDone = false;

    /**
     * Width and Heigh are kept, but are not actively used.
     * Consider removing.
     */
    private int width = 0;
    private int height = 0;

    private float startingEnergy;

    /**
     * Initialize a driver with a robot.
     * @param r is the robot the driver will be driving
     */
    public WallFollower(Robot r) {
        this.robbie = r;
        this.setDimensions(this.robbie.getWidth(), this.robbie.getHeight());
        this.startingEnergy = this.robbie.getBatteryLevel();
    }

    /**
     * Initialize Wally without any information.
     */
    public WallFollower() {
        // nothing to do here
    }

    /**
     * Set a robot to Wally and update needed information accordingly
     */
    @Override
    public void setRobot(Robot r) {
        this.robbie = r;
        this.setDimensions(this.robbie.getWidth(), this.robbie.getHeight());
        this.startingEnergy = this.robbie.getBatteryLevel();
    }


    @Override
    public void setDimensions(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Wally shines his powers HERE.
     * NOTE: WALLY only uses the left and right sensors, but because i don't want
     * to make a new robot, i am just gonna ignore them.
     * First, rotate to make sure there is a wall on his left side and that Wally can move forwards.
     * Whiles Wally is not at the exit, he'll keep driving with a wall on his left side.
     * If Wally understandably hits a wall, he'll turn right until there is not a wall in front of him,
     * and then follow that wall so it is on his left side.
     *
     * If there are no walls, turn left and move forward ((MAYBE idrk yet))
     * If there is a wall on the left and the front, turn right
     * If there is only a wall on the left and no wall forward, move
     *
     * If there is only a wall in front check some things:
     * * * if the last move was a move forward along a left wall, perform a U Turn
     *
     *
     * @return true if wally finishes
     * @return false if wally doesn't finish before running out of energy.
     */
    @Override
    public boolean drive2Exit() throws Exception {
        Log.v("drive2Exit", "Making another step for Wally");
        boolean atExit = this.robbie.isAtExit();

        if(!atExit && this.robbie.getBatteryLevel() > this.robbie.getEnergyForStepForward()) {

            boolean [] walls = this.robbie.borderWalls();
            // it's easier for me to code if i don't have to use an array. I'm going to parse them out here
            // completely unnecessary logic wise - just nice for me.

            boolean frontWall = walls[0];
            boolean leftWall = walls[3];

            if(!firstOneDone){
                firstOneDone = true;
            }
            else if(uTurn > 0){
                if(uTurn == 1){
                    this.robbie.rotate(Turn.LEFT);
                    uTurn++;
                }
                else if(uTurn == 2){
                    this.robbie.move(1, false);
                    uTurn++;
                }
                else if(uTurn == 3){
                    if(!this.robbie.borderWalls()[3]){
                        this.robbie.rotate(Turn.LEFT);
                        uTurn++;
                    }
                    else{
                        uTurn = 0;
                    }
                }
                else if(uTurn == 4){
                    this.robbie.move(1, false);
                    uTurn = 0;
                }
            }
            else if(needsToMove){
                robbie.move(1, false);
                this.needsToMove = false;
                lastMoveLeft = true;
            }
            // if there is no front wall and there is a left wall, move forward
            else if(!frontWall && leftWall) {
                this.needsToMove = true;
                lastMoveLeft = true;

            }
            // if you just moved and ran out of left wall, do a u turn
            else if(lastMoveLeft && !leftWall) {
                this.uTurn();
                lastMoveLeft = false;
            }
            // if there is both a front wall and a left wall, turn right
            else if(leftWall && frontWall) {
                robbie.rotate(Turn.RIGHT);
                lastMoveLeft = false;
            }
            // if there is ONLY a right wall, rotate left
            else if(!leftWall && !frontWall) {
                robbie.rotate(Turn.LEFT);
                lastMoveLeft = false;
            }
            // if you just moved past a left wall and have hit a border, do a u turn
            else if(lastMoveLeft && frontWall && !leftWall) {
                this.uTurn();
                lastMoveLeft = false;
            }
            // if you just moved past a left wall and have hit a border, do a u turn
            else if(lastMoveLeft && frontWall) {
                this.uTurn();
                lastMoveLeft = false;
            }
            // if there are no walls, turn left and move
            else if(!frontWall  && !leftWall) {
                robbie.rotate(Turn.LEFT);
                needsToMove = true;
            }
            // if there is only a front and right wall, turn right
            else if(frontWall && !leftWall) {
                robbie.rotate(Turn.RIGHT);
                lastMoveLeft = false;
            }

            // check to see if you're at the exit!
            atExit = this.robbie.isAtExit();
        }

        if(atExit) {
            System.out.println("Drove Wally to the Exit");
        }
        else {
            System.out.println("Did not drive Wally to the Exit");
        }

        return atExit;
    }

    @Override
    public float getEnergyConsumption() {
        return this.startingEnergy - this.robbie.getBatteryLevel();
    }

    @Override
    public int getPathLength() {
        return robbie.getOdometerReading();
    }

    @Override
    public void wakeUp() {
        System.out.println("Waking up the Wally.");
        this.setDimensions(this.robbie.getWidth(), this.robbie.getHeight());

    }

    // todo get rid of this. keeping it for reference rn
    public boolean keyDown(UserInput key) {
        if(!robbie.isPlaying()) {
            return false;
        }
        switch(key) {
            case Up:
                System.out.println("Wally is Driving towards the Exit");
                try {
                    this.drive2Exit();
                    this.robbie.rotateToViewExit();
                    this.robbie.move(1, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
        return false;
    }

    /**
     * Make robbie try to do a u turn.
     * IF robbie turns and moves and doesn't sense a left wall
     * after the move, turn left again and then move
     *
     * otherwise, just stay looking straight.
     */
    public void uTurn() {
        uTurn = 1;
    }

}
