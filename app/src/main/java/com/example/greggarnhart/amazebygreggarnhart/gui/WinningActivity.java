package com.example.greggarnhart.amazebygreggarnhart.gui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.VibrationEffect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.os.Vibrator;

import com.example.greggarnhart.amazebygreggarnhart.R;

import generation.GlobalClass;

public class WinningActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winning);

        // Added a Vibration Effect
        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
        Log.v("Vibrate Module", "Vibrated!");

        GlobalClass gc = (GlobalClass) getApplicationContext();
        if(gc.nightMode){
            nightMode();
        }

        Intent past = getIntent();
        Bundle extras = past.getExtras();
        Boolean didUseRobot = extras.getBoolean(PlayAnimationActivity.ROBOT_USED); // try this, we will see if it works lol

        if(didUseRobot){
            TextView energyConsumptionLabel = (TextView) findViewById(R.id.energyConsumptionLabel);
            String amountUsed = past.getStringExtra(PlayAnimationActivity.CELLS_CONSUMED);
            energyConsumptionLabel.setText("Your driver used "+amountUsed+" energy cells. Hope you left them a tip!");

            TextView stepsLabel = (TextView) findViewById(R.id.stepsLabel);
            String stepsUsed = past.getStringExtra(PlayAnimationActivity.STEPS_TAKEN);
            String stepsNeeded = past.getStringExtra(PlayAnimationActivity.SHORTEST_PATH);
            if(stepsUsed.equals(stepsNeeded)){
                stepsLabel.setText("Hey your driver figured out the maze's shortest path! You finished in "+stepsUsed+" moves."); // TODO: Fix moves so that it is really steps bc shortest path dose not account for turns
            }
            else{
                stepsLabel.setText("I would walk 500 miles to wind up at your door, but your driver only needed "+stepsUsed+" steps. Nice!");
            }

        }
        else{
            TextView energyConsumptionLabel = findViewById(R.id.energyConsumptionLabel);
            energyConsumptionLabel.setVisibility(View.INVISIBLE);

            TextView stepsLabel = (TextView) findViewById(R.id.stepsLabel);
            String stepsUsed = past.getStringExtra(PlayManually.STEPS_TAKEN);
            String stepsNeeded = past.getStringExtra(PlayManually.SHORTEST_PATH);

            if(stepsUsed.equals(stepsNeeded)){
                stepsLabel.setText("Hey you figured out the maze's shortest path! You finished in "+stepsUsed+" moves."); // TODO: Fix moves so that it is really steps bc shortest path dose not account for turns
            }
            else{
                stepsLabel.setText("It took you "+stepsUsed+" steps to get to the finish. That's almost as many steps as I took while coding this project!");
            }

        }
    }

    private void nightMode() {
        int deepBlue = Color.parseColor("#00303D");
        getWindow().getDecorView().setBackgroundColor(deepBlue);

        String fontColor = "#F3F3F3";
        String buttonColor = "#37CFCA";

        int fontColorInt = Color.parseColor(fontColor);
        int buttonColorInt = Color.parseColor(buttonColor);

        Button back = findViewById(R.id.backToMenu);
        TextView blurb = findViewById(R.id.blurb);
        TextView energy = findViewById(R.id.energyConsumptionLabel);
        TextView steps = findViewById(R.id.stepsLabel);


        back.setBackgroundColor(buttonColorInt);
        blurb.setTextColor(fontColorInt);
        energy.setTextColor(fontColorInt);
        steps.setTextColor(fontColorInt);

    }

    public void backToMenu(View view){
        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);

        Log.v("Back to Menu Button", "Button Tapped, moving to menu!");
    }
}
