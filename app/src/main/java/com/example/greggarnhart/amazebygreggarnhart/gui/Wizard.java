package com.example.greggarnhart.amazebygreggarnhart.gui;

import android.util.Log;

import generation.CardinalDirection;
import generation.Distance;
import generation.GlobalClass;
import generation.MazeConfiguration;
import com.example.greggarnhart.amazebygreggarnhart.gui.Constants.UserInput;
import com.example.greggarnhart.amazebygreggarnhart.gui.Robot.Direction;
import com.example.greggarnhart.amazebygreggarnhart.gui.Robot.Turn;

/**
 *
 * @author greggarnhart
 * The Wizard Driver is magical. It knows exactly where to go, how to get there, and then drives itself.
 * If I could do this, I'd never get lost!
 * I can't do this, though.
 *
 * TO THE GRADER: PRESS THE UP ARROW TO START THE DRIVE TO EXIT METHOD
 *
 * Anyways, here is some CRC info:
 * Responsibilities:
 * - Looks for neighbor closest to exit
 * - Goes to exit and that's really it tbh
 * - Hint: you find similar functionality implemented in the MazeContainer.getNeighborCloserToExit()
 *         to support drawing the yellow line that shows the path to the exit)
 *
 * Collaborators:
 * - RobotDriver Interface
 * - Some sort of Robot (maybe basic robot, maybe not)
 * - WizardTest (this is a generous 'collaborator' but so i remember...)
 *
 * To the Graders:
 * Get ready for a lot of Wizard jokes.
 *
 */
public class Wizard implements RobotDriver {

    /**
     * Robbie is the robot that the wizard drives.
     * It can technically be any type of Robot, but the basic robot will work perfectly, so we'll do that.
     */
    private Robot robbie;

    /**
     * Basic width and height properties of the maze. Not used in Wizard
     */
    private int width = 0;
    private int height = 0;

    private float startingEnergy;
    private MazeConfiguration mazeConfiguration;

    /**
     * Initialize a driver with a robot.
     * @param r is the robot the driver will be driving
     */
    public Wizard(Robot r, MazeConfiguration mazeConfiguration) {
        this.robbie = r;
        this.mazeConfiguration = mazeConfiguration;
        this.setDimensions(mazeConfiguration.getWidth(), mazeConfiguration.getHeight());
        this.startingEnergy = this.robbie.getBatteryLevel();
    }

    /**
     * Initialize a wizard without any information.
     */
    public Wizard() {
        // nothing to do here
    }

    /**
     * Set a robot to the wizard and update needed information accordingly
     */
    @Override
    public void setRobot(Robot r) {
        this.robbie = r;
        this.setDimensions(this.robbie.getWidth(), this.robbie.getHeight());
        this.startingEnergy = this.robbie.getBatteryLevel();
    }


    @Override
    public void setDimensions(int width, int height) {
        this.width = width;
        this.height = height;
    }


    /**
     * This is where the magic happens.
     * while the robot is not at the exit & it has energy, make it keep driving.
     * The wizard will use something v similar to check which neighbor is closest to the exit and go to that one
     *
     * I suppose it can drive through the exit too.
     *
     * Anyways, at every point, check the position to see if it is the exit and if not, where the closest neighbor
     * to the exit is.
     * Then, if you need to, rotate to face that cell and go there.
     *
     * That's really it! yay.
     *
     * @return true - if the robot makes it to (through?) the exit
     * @return false - if the robot did not make it to the exit due to energy constraints
     */
    @Override
    public boolean drive2Exit() throws Exception {
        Log.v("drive2Exit", "Driving Wiz to Exit");
        boolean atExit = this.robbie.isAtExit();



        if(!atExit && this.robbie.getBatteryLevel() > this.robbie.getEnergyForStepForward()) {
            int [] position = this.robbie.getCurrentPosition();
            int [] closestNeighborCell = mazeConfiguration.getNeighborCloserToExit(position[0], position[1]);

            // now, figure out if we are facing that direction (e.g. is that point to our west?)
            // if we can see it, move forward!
            if(canSeePoint(closestNeighborCell)) {
                this.robbie.move(1, false);
            }
            // determine the type of rotation that needs to take place and rotate accordingly
            // move forward
            else {
                this.robbie.rotate(whichRotate(closestNeighborCell));
                this.robbie.move(1, false);
            }

            // check to see if you're at the exit!

            if(this.robbie.isAtExit()){
                atExit = this.robbie.isAtExit();
                Log.v("drive2Exit", "Made it ;)");
            }
            else{
                atExit = this.robbie.isAtExit();
            }
        }

        if(!atExit) {
            System.out.println("Wiz did not make it to the exit.");
        }
        return atExit;
    }

    @Override
    public float getEnergyConsumption() {
        return this.startingEnergy - this.robbie.getBatteryLevel();
    }

    @Override
    public int getPathLength() {
        return robbie.getOdometerReading();
    }

    @Override
    public void wakeUp() {
        System.out.println("Waking up the Wizard.");
        this.setDimensions(this.robbie.getWidth(), this.robbie.getHeight());

    }

    // todo: make this work with the up button? i guess? idk HA ha.
    public boolean keyDown(UserInput key) {
        if(!robbie.isPlaying()) {
            return false;
        }
        switch(key) {
            case Up:
                System.out.println("Wiz is Driving towards the Exit");
                try {
                    this.drive2Exit();
                    this.robbie.rotateToViewExit();
                    this.robbie.move(1, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
        return false;
    }

    /**
     * Used to determine whether or not a rotation needs to take place
     * May end up deleting this - idk if it is all that useful.
     * @param point is the next point the driver will be going to
     * @return true if the point is forward
     * @return false if the point is not ahead of the robot, but instead to one of the sides or behind it.
     * @throws Exception if robbie is out of bounds. We just won't call it then :)
     */
    private boolean canSeePoint(int [] point) throws Exception {

        CardinalDirection viewingDirection = this.robbie.getCurrentDirection();
        int [] currentPoint = this.robbie.getCurrentPosition();

        switch(viewingDirection) {
            // to the east or the west, the x will change (pos[0])
            // to the north or the south, the y will change (pos[1])
            case North:
                if(currentPoint[0] == point[0]) {
                    return currentPoint[1] > point[1];
                }
                return false;
            case East:
                if(currentPoint[1] == point[1]) {
                    return currentPoint[0] < point[0];
                }
                return false;
            case West:
                if(currentPoint[1] == point[1]) {
                    return currentPoint[0] > point[0];
                }
                return false;
            case South:
                if(currentPoint[0] == point[0]) {
                    return currentPoint[1] < point[1];
                }
                return false;
            default:
                return false;
        }
    }


    /**
     * Determines which way the robot needs to rotate to move to the next point.
     * @param point is the point the robot wants to face.
     * @return is the way the robot needs to turn.
     * @throws Exception is thrown when the robot is out of bounds.
     */
    private Turn whichRotate(int [] point) throws Exception{
        CardinalDirection facing = this.robbie.getCurrentDirection();
        int [] cur = this.robbie.getCurrentPosition();

        switch(facing) {

            case North:
                if(cur[0] == point[0]) {
                    return Turn.AROUND;
                }
                else {
                    if(cur[0] < point[0]) {
                        return Turn.LEFT;
                    }
                    else if(cur[0] > point[0]){
                        return Turn.RIGHT;
                    }
                }

            case East:
                if(cur[1] == point[1]) {
                    return Turn.AROUND;
                }
                else {
                    if(cur[1] > point[1]) {
                        return Turn.RIGHT;
                    }
                    else if(cur[1] < point[1]) {
                        return Turn.LEFT;
                    }
                }

            case West:
                if(cur[1] == point[1]) {
                    return Turn.AROUND;
                }
                else {
                    if(cur[1] > point[1]) {
                        return Turn.LEFT;
                    }
                    else if(cur[1] < point[1]) {
                        return Turn.RIGHT;
                    }
                }

            case South:
                if(cur[0] == point[0]) {
                    return Turn.AROUND;
                }
                else {
                    if(cur[0] < point[0]) {
                        return Turn.RIGHT;
                    }
                    else if(cur[0] > point[0]){
                        return Turn.LEFT;
                    }
                }

            default:
                return Turn.AROUND;
        }
    }
}

