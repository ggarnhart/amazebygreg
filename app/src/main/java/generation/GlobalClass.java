package generation;

import android.app.Application;

public class GlobalClass extends Application {
    public MazeConfiguration mazeConfiguration;
    public MazeFactory mazeFactory;
    public boolean writtenSeeds = false;

    public boolean nightMode = false;
}
