package generation;

import java.util.Random;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.HashMap;
import java.util.HashSet;

public class MazeBuilderEller extends MazeBuilder implements Runnable {
    /*
     * Greg's Private Variables <3
     */
    protected List<List<List<Integer>>> cellSetList;
    private int rooms;
    static final int MAX_TRIES = 250 ; // room generation: max number of tries to find a random location for a room
    static final int MIN_ROOM_DIMENSION = 3; // room generation: min dimension
    static final int MAX_ROOM_DIMENSION = 8; // room generation: max dimension

    public MazeBuilderEller() {
        super();
        System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");
    }

    public MazeBuilderEller(boolean det) {
        super(det);
        System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");
    }


    public void setUp() {
        fillSet();
    }

    /*
     * Used to fill the set with all of the pairs of cells.
     * That is, it will go through and add things like {0,0}, {0,1}, ...{10,10}...{height-1, width-1}
     *
     * When finished, cellSetList will have a list of lists. The embedded lists each contain the coordinates of those
     * cells that have the same value :)
     *
     * Not sure why 'sets' are used in the example. This makes sense to me!
     *
     * Also, these pairs will be added in (x,y) format.
     */
    public void fillSet() {
        cellSetList = new ArrayList<>();
        for(int r = 0; r<this.width; r++) {
            for(int c = 0; c<this.height; c++) {
                List<Integer> temp = new ArrayList<Integer>();
                List<List<Integer>> curSet = new ArrayList<List<Integer>>();
                temp.add(r);
                temp.add(c);
                curSet.add(temp);
                cellSetList.add(curSet);
            }
        }
    }



    /*
     * This method generates pathways into the maze by using Eller's algorithm
     * It goes row by row - calling step() to break the walls down
     */
    @Override
    public void generatePathways() {
        this.setUp();
        for(int row = 0; row < this.height; row++) {
            if(row != this.height-1) {
                this.step(row);
            }
            else {
                this.lastRow();
            }
        }
    }

    /*
     * merge the subordinate into the parent
     * One can assume the setter will be the smaller of the two.
     * Parameters: 2 cells' coordinates.
     *
     * Intended result at the END of the program: ONE list.
     */
    public void merge(ArrayList <Integer> setter, ArrayList <Integer> settee) {
//		this.printCells();
//		this.printCellMap();
//		System.out.println("Pre Merge: "+this.cellSetList);
//		System.out.println("Cell Set List Count: "+ this.countAllCoordinatePairs());
        if(setter == settee) {
//			System.out.println("Merge Error: Setter == settee");
        }
        List<List<Integer>> setterList = null;
        List<List<Integer>> setteeList = null;
        List<List<Integer>> setterListBackup = null;
        List<List<Integer>> setteeListBackup = null;
        /*
         * Searching through my arrayList of arrayLists is not efficient.
         * I don't think it matters, though.
         */

        int setterIndex = 0;
        while(setterList == null) {
            List<List<Integer>> temp = this.cellSetList.get(setterIndex);
            if(temp.contains(setter)) {
                setterList = temp;
                setterListBackup = temp;
            }
            setterIndex++;
        }

        int setteeIndex = 0;
        while(setteeList == null) {
            List <List<Integer>> temp = this.cellSetList.get(setteeIndex);
            if(temp.contains(settee)) {
                setteeList = temp;
                setteeListBackup = temp;
            }
            setteeIndex++;
        }

        /*
         * If the count of lists is 2, then just merge them.
         */
        if(this.cellSetList.size() == 2) {
            setterList = this.cellSetList.get(0);
            setteeList = this.cellSetList.get(1);

            for(List<Integer> current : setteeList) {
                setterList.add(current);
                setteeList.remove(current);
            }
        }


        // we have both lists
        // now, change the values of the map in the settee coordinate list
        // and add the settee coordinates to the setter list
        boolean switchList = false;
        if(!(setteeList.size() <= setterList.size())) {
//			System.out.println("hey we are using that list thing");
//			System.out.println("Setter:" + setter);
//			System.out.println("Settee:" + settee);
            List<List<Integer>> placeholderList = setteeList;
            setteeList = setterList;
            setterList = placeholderList;
            switchList = true;
        }
        int startSize = setteeList.size();
        for(int i = 0; i < startSize; i++) {
            List<Integer> curLocation = setteeList.get(0);
            setterList.add(curLocation); // update the setter list with the coordinate
            setteeList.remove(0); // remove the coordinate from the settee List

        }

        // remove the list of settee coordinates from the overarching cellListSets
        if(setteeIndex > 0 && !switchList) {
            this.cellSetList.remove(setterListBackup);
            this.cellSetList.remove(setteeListBackup);
        }
        else if (switchList) {
//			List<List<List<Integer>>> newCellList = new ArrayList<List<List<Integer>>>();
//			for(int i = 0; i<)


            this.cellSetList.remove(setterListBackup);
            this.cellSetList.remove(setteeListBackup);
        }

        // finally, add the setter list to the front.
        // this should do it.
        this.cellSetList.add(0, setterList);
//		System.out.println("Post Merge: "+this.cellSetList);
//		System.out.println();
    }


    /*
     * Used when determining whether or not knocking down a wall makes a good maze.
     * If they are in the same set, then that means they're connected somewhere.
     * In step(), it works so that if they are the same(), then it skips over it.
     */
    public boolean same(int xCoordOne, int yCoordOne, int xCoordTwo, int yCoordTwo) {
        ArrayList<Integer> setter = new ArrayList<Integer>();
        setter.add(xCoordOne);
        setter.add(yCoordOne);

        List<List<Integer>> temp = null;
        int count = 0;
        while(temp == null) {
            List<List<Integer>> sets = cellSetList.get(count);
            if(sets.contains(setter)) {
                temp = cellSetList.get(count);
            }
            count++;
        }

        List<Integer> settee = new ArrayList<Integer>();
        settee.add(xCoordTwo);
        settee.add(yCoordTwo);

        return temp.contains(settee);
    }


    /*
     * Allows for each row to be done one by one
     * walls are broken down for EAST and SOUTH only
     * A friendly reminder that cells are in an (x,y) pattern and NOT a [row][column] pattern
     *
     */
    public void step(int currentRow) {
        // we go row by row. Don't need a row variable (that's sent in), but we do need a col var
        int col = 0;
        Random rand = new Random();

        Wall curWall; // wall that will eventually be torn down
        ArrayList<Wall> eastWalls = new ArrayList<Wall>();
        ArrayList<Wall> southWalls = new ArrayList<Wall>();


        // don't put the border wall on the list
        // friendly reminder that the coordinates are (x, y) and that x is a column
        for(col = 0; col < width-1; col++) {

            curWall = new Wall(col, currentRow, CardinalDirection.East);
            eastWalls.add(curWall);

            curWall = new Wall(col, currentRow, CardinalDirection.South);
            southWalls.add(curWall);
        }

        boolean eastWallDown = false; // make sure at least one wall is knocked down.



        /*
         * First, let's knock down the horizontal walls
         */
        for(col = 0; col < this.width-1; col++) {
            // update the wall that we might be knocking down.
            curWall = eastWalls.get(col);

            boolean knock = rand.nextBoolean();
//			System.out.println("knock came in as "+knock);
            if(knock && cells.hasWall(col, currentRow, CardinalDirection.East) && !same(col, currentRow, col+1, currentRow)) {
                cells.deleteWall(curWall);
                eastWallDown = true;

                ArrayList<Integer> setter = new ArrayList<Integer>();
                ArrayList<Integer> settee = new ArrayList<Integer>();

                setter.add(col);
                setter.add(currentRow);

                settee.add(col+1);
                settee.add(currentRow);

                this.merge(setter, settee);
            }
            if(!eastWallDown && col == ((this.width)-1)) {
                col = 0; // try loop through again. This is inefficient as hell, but it shouldn't happen that often
            }
        }



        Set<List<List<Integer>>> rowSetCount = this.getSetCount(currentRow);

        // basically a safety check
        // this happens if NO merges happened on the previous row.
        if(rowSetCount.size() == this.width){
            System.out.println("Big issue encountered: Did not merge properly on previous row. rowSetCount == this.width");
        }

        /*
         * Now, let's do the big time vertical deletions
         * We need at least one vertical path in between each set in a row.
         * The rowSetCount specifies the coordinates on the row.
         *
         * Implementation:
         * Use the set lists to do coordinates.
         */

        if(currentRow < this.height-1) {
            /*
             * Start by cycling throw the set in a list by list format
             */
            for(List<List<Integer>> currentSet : rowSetCount) {
                // Nice! Now we have a list of lists. We'll need to cycle through that as well

                boolean setKnockedDownAWall = false;

                /*
                 * If it's only one cell, then we NEED to put a path to the next one.
                 */
                if(currentSet.size() == 1) {
                    this.knockDownSouthernWall(currentSet, 0);
                    setKnockedDownAWall = true;
                }
                else {
                    // otherwise, go through the elements of the list.
                    for(int i = 0; i < currentSet.size(); i++) {
                        List<Integer> temp = currentSet.get(i);

                        if(temp.get(1) == currentRow && rand.nextBoolean() && !setKnockedDownAWall) {
                            this.knockDownSouthernWall(currentSet, i);
                            setKnockedDownAWall = true;
                        }

                        if(!setKnockedDownAWall && i == currentSet.size() -1) {
                            // as always, this is inefficient, but efficiency is not what I'm going for so.
                            i = 0;
                        }
                    }
                }

                if(!setKnockedDownAWall) {
                    System.out.println("Big issue: Failed to knock down a south wall when we needed to");
                }
            }
        }
    }

    /*
     * Returns the amount of unique numbers in a row.
     */
    protected Set<List<List<Integer>>> getSetCount(int row) {
        // use row and then a made up column number to go through this.
        // given the coordinates, add whatever list the coordinate is on.
        Set<List<List<Integer>>> numbers = new HashSet<List<List<Integer>>>();
        List<List<Integer>> list = null;
        for(int col = 0; col < this.width; col++) {
            list = findList(col, row);
            List<List<Integer>> editedList = new ArrayList<List<Integer>>();
            for(List<Integer> curr : list) {
                if(curr.get(1) == row) {
                    editedList.add(curr);
                }
            }
            numbers.add(editedList); // SHOULD only return a list with the correct row in it.
        }



        // System.out.println(numbers);
        return numbers;
    }

    /*
     * Returns the set (list) that the coordinate is in.
     * ONLY used in getSetCount
     */
    protected List<List<Integer>> findList(int x, int y){
        List<List<Integer>> temp = null;
        List<Integer> setter = new ArrayList<Integer>();
        setter.add(x);
        setter.add(y);

        int count = 0;
        while(temp == null) {
            List<List<Integer>> sets = cellSetList.get(count);
            if(sets.contains(setter)) {
                temp = cellSetList.get(count);
            }
            count++;
        }

        return temp;
    }

    protected void knockDownSouthernWall(List<List<Integer>> currentSet, int index) {
        List<Integer> coordinates = currentSet.get(index);
        Wall deleteMe = new Wall(coordinates.get(0), coordinates.get(1), CardinalDirection.South);
        this.cells.deleteWall(deleteMe);

        // unfortunately, the coordinates are currently in a List & not an ArrayList,
        // so we'll need to make two arrayLists. Wasteful and dumb, but that's for a dev
        // with more time to worry about.
        ArrayList<Integer> setter = new ArrayList<Integer>();
        setter.add(coordinates.get(0));
        setter.add(coordinates.get(1));


        ArrayList<Integer> settee = new ArrayList<Integer>();
        settee.add(coordinates.get(0));
        int y = coordinates.get(1);
        y = y+1;
        settee.add(y);

        this.merge(setter, settee);
    }

    protected int countAllCoordinatePairs() {
        //protected List<List<List<Integer>>> cellSetList;
        int count = 0;
        for(List<List<Integer>> one : this.cellSetList) {
            for(List<Integer> two : one) {
                count++;
            }
        }
        return count;
    }

    /*
     * In Eller's algorithm, the last Row sort of acts as a "catch all"
     * The last walls that need to be broken down are broken down
     * and no vertical walls are broken down (obviously)
     *
     * I could probably do this in my original step() function, but it seems like a special enough case
     * to do it here.
     *
     * Enjoy :)
     */
    protected void lastRow() {
        /*
         *
         * Just a PSA: Trying this out without using "Merge"
         * It's the last row, the walls will be knocked down, and we don't really need to verify anything
         * (assuming it works)
         * Here we go;)
         *
         */
        // first, count the sets in the last row. Nice!
        if(this.cellSetList.size() == 1) {
            // well in this case, we're already done. idk, print "yay!"
        }
        else {
            // oh look we have a nice function
            Set<List<List<Integer>>> sets = this.getSetCount(this.height -1);
            for(List<List<Integer>> currentOrdeal : sets) {
                if(currentOrdeal.size() == 1) {
                    Wall w;
                    List<Integer> coordinates = currentOrdeal.get(0);
                    ArrayList<Integer> neighbor = new ArrayList<Integer>();
                    ArrayList<Integer> selfCoord =new ArrayList<Integer>();

                    selfCoord.add(coordinates.get(0));
                    selfCoord.add(coordinates.get(1));
                    if(coordinates.get(0) == this.width-1) {
                        w = new Wall(coordinates.get(0), coordinates.get(1), CardinalDirection.West);
                        neighbor.add(coordinates.get(0)-1);
                        neighbor.add(coordinates.get(1));

                        if(this.cells.hasWall(coordinates.get(0), coordinates.get(1), CardinalDirection.West)) {
                            this.cells.deleteWall(w);
                            //this.merge(selfCoord, neighbor);
                        }
                    }
                    else {
                        w = new Wall(coordinates.get(0), coordinates.get(1), CardinalDirection.East);
                        neighbor.add(coordinates.get(0)+1);
                        neighbor.add(coordinates.get(1));
                        if(this.cells.hasWall(coordinates.get(0), coordinates.get(1), CardinalDirection.East)) {
                            this.cells.deleteWall(w);
                            //this.merge(selfCoord, neighbor);
                        }
                    }
                }
                else {
                    // in this case, the set is big and expansive ON this row.
                    // we'll need to find EITHER the max or min column value and break the wall on that. :)
                    int maxCol = 1;
                    int minCol = 1;
                    ArrayList<Integer> minCoords = new ArrayList<Integer>();
                    ArrayList<Integer> maxCoords = new ArrayList<Integer>();
                    for(List<Integer> c : currentOrdeal) {
                        if(minCol > c.get(0)) {
                            minCol = c.get(0);
                            minCoords.add(c.get(0));
                            minCoords.add(this.height-1);
                        }

                        if(maxCol < c.get(0)) {
                            maxCol = c.get(0);
                            maxCoords.add(c.get(0));
                            maxCoords.add(this.height-1);
                        }
                    }

                    // we'll prioritize breaking east.
                    Wall w;
                    ArrayList<Integer> neighbor = new ArrayList<Integer>();
                    ArrayList<Integer> selfCoord =new ArrayList<Integer>();
                    if(maxCol < this.width-1) {
                        w = new Wall(maxCoords.get(0), maxCoords.get(1), CardinalDirection.East);
                        neighbor.add(maxCoords.get(0)+1);
                        neighbor.add(maxCoords.get(1));
                        if(this.cells.hasWall(maxCoords.get(0), maxCoords.get(1), CardinalDirection.East)) {
                            this.cells.deleteWall(w);
                            //this.merge(selfCoord, neighbor);
                        }
                    }
                    else {
                        w = new Wall(minCoords.get(0), minCoords.get(1), CardinalDirection.West);
                        neighbor.add(minCoords.get(0)+1);
                        neighbor.add(minCoords.get(1));
                        if(this.cells.hasWall(minCoords.get(0), minCoords.get(1), CardinalDirection.East)) {
                            this.cells.deleteWall(w);
                            //this.merge(selfCoord, neighbor);
                        }
                    }
                }
            }
        }
    }

    /*
     * Hello!
     * I do not need to do keep these functions as rooms are created well without these,
     * but I wanted to make it clear that I did not forget so much as I chose to use the default :)
     *
     * Thanks,
     * Greg
     */

//	/*
//	 * returns the number of rooms that were sucessfully placed :)
//	 * Slightly different than the normal placeRooms() method.
//	 */
//	protected int placeRooms() {
//		int result = 0;
//		int triesLeft = MAX_TRIES;
//		while(triesLeft > 0 && result < this.rooms) {
//			if(placeRoom()) {
//				result++;
//				triesLeft--;
//			}
//			else {
//				triesLeft--;
//			}
//		}
//		return result;
//	}
//
//	/*
//	 * Tries to place a room in the cells
//	 * Note, this happens before everything else.
//	 * We start by taking a corner of the room which is generated taking a random number from the width and height
//	 * Then, we make sure that all the cells are in lists of their own.
//	 * We do this to make sure we are not invading on other rooms.
//	 */
//	protected boolean placeRoom() {
//		boolean roomPlaced = false;
//		Random r = new Random();
//		int startingPostionX = r.nextInt(this.width);
//		int startingPostionY = r.nextInt(this.height);
//
//		final int rw = random.nextIntWithinInterval(MIN_ROOM_DIMENSION, MAX_ROOM_DIMENSION);
//		if (rw >= width-4)
//			return false;
//
//		final int rh = random.nextIntWithinInterval(MIN_ROOM_DIMENSION, MAX_ROOM_DIMENSION);
//		if (rh >= height-4)
//			return false;
//
//
//		return roomPlaced;
//	}
}
